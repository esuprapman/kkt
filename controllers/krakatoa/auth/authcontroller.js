const moment = require('moment-timezone');
const format2 = "YYMMDDHHmmss";
const _period = "YMM";
const formatISO = "YYYY-MM-DDTHH:mm:ss";
const pool = require("../../../connection/db.js");
const jwttools = require("../../../utils/encryptdecryptjwt");
const kktResp = require("../../../models/kkt_resp");
const { checkPassword } = require('../../../utils/passwordutils.js');


const signVia = async (req, res, next) => {
    try {
        const { credential, secret, ldap } = req.body;
        let result = {};
        // 1. CHECK ldap Type (0= local, 1= ldap, 2=uid, ......)
        switch (ldap) {
            case 0:
                console.log("####### LOCAL LOGIN ########");
                //############ GET USER On Database ######################
                const users = await pool.select("ku.*", "mt.name_tenant", "mt.desc_tenant", "mt.address_tenant", "mt.phone_tenant", "mt.email_tenant").from("kkt_mst_tenant_user as ku").innerJoin("kkt_mst_tenant as mt", "ku.id_tenant_user", "mt.id_tenant").where({ "ku.keyid_user": credential }).orderBy("ku.created_at", "DESC");
                let status = users.length > 0 ? 200 : 202;
                //#########################CHECK IF 200 but password failed give cookie but with filed response 
                if (status == 200) {
                    let user = users[0];
                    const checkingP = await checkPassword(user, secret, res);
                    if (checkingP.status) {
                        return new kktResp(status, { data: checkingP.data });
                    } else {
                        return new kktResp(202, { data: checkingP.data });
                    }
                } else {
                    return new kktResp(203, { data: `No User ${credential} founded!` });
                }
                //#################################################
                break;
            case 1:
                return new kktResp(203, { message: "LDAP Auth Not Set!" });;
                break;
            default:
                return new kktResp(203, { message: "No Login Type Identified" });
                break;
        }

    } catch (error) {
        console.log(error);
        return new kktResp(500, error);
    }
}
// console.log("ADA GROUPNYA NIH ", JSON.stringify(objectApp));
// console.log("GROUP RECORD ", JSON.stringify(gropObject));
const signViaKeyApp = async (req, res, next) => {
    try {
        const { credential, secret, ldap, objectApp } = req.body;
        let result = {};
        // 1. CHECK ldap Type (0= local, 1= ldap, 2=uid, ......)
        console.log("SIGN IN VIA APP");
        switch (ldap) {
            case 0:
                console.log("####### LOCAL LOGIN KEY APP ######## ", credential);
                //############ GET USER On Database ######################
                const users = await pool.select("ku.*", "mt.name_tenant", "mt.desc_tenant", "mt.address_tenant", "mt.phone_tenant", "mt.email_tenant").from("kkt_mst_tenant_user as ku").innerJoin("kkt_mst_tenant as mt", "ku.id_tenant_user", "mt.id_tenant").where({ "ku.keyid_user": credential }).orderBy("ku.created_at", "DESC");
                let status = users.length > 0 ? 200 : 202;
                //#########################CHECK IF 200 but password failed give cookie but with filed response 
                if (status == 200) {
                    let user = users[0];
                    const groupRecords = await pool.select("kgu.*", "mg.id_app_group").from("kkt_set_group_user as kgu").innerJoin("kkt_mst_group as mg", "kgu.id_group_set_group", "mg.id_group").where({ "kgu.id_user_set_group": user.id_user }).orderBy("kgu.created_at", "DESC");

                    console.log("BANYAKNYA Module GROUP ",groupRecords.length);

                    // console.log("Object Applicaion ",JSON.stringify(objectApp));
                    if (groupRecords.length > 0) {
                        let moduleMenus = [];
                        let gropObject = groupRecords[0];
                        if (objectApp.id_app === gropObject.id_app_group) {
                            console.log("####### GET GROUPS MODULES ######## ", gropObject.id_group_set_group);
                            const modulesRec = await pool.select("kmgd.id_seq_gd", "kmgd.id_module_gd", "kmgd.id_menus_gd", "kmam.icon_module", "kmam.children_menu", "kmam.name_module", "kmam.desc_module").from("kkt_mst_group_detail as kmgd").innerJoin("kkt_mst_apps_modules as kmam", "kmgd.id_module_gd", "kmam.id_module").where({ "kmgd.id_header_gd": gropObject.id_group_set_group });
                            if (modulesRec.length > 0) {
                                await Promise.all(
                                    modulesRec.map(async element2 => {
                                        var arrayMenu = JSON.parse("[" + element2.id_menus_gd + "]");
                                        menus = await pool.select("kgm.*").from("kkt_mst_apps_menus as kgm").whereIn("id_menu", arrayMenu);
                                        delete element2['id_menus_gd']
                                        element2 = { ...element2, menus };
                                        moduleMenus.push(element2);
                                    })
                                )
                                user = { ...user, ...gropObject, moduleMenus };
                            } else {
                                user = { ...user, ...gropObject, moduleMenus };
                            }
                            // user = {...user, ...gropObject, modulesRec};
                            const checkingP = await checkPassword(user, secret, res);
                            if (checkingP.status) {
                                let result = { data: checkingP.data, group: objectApp }
                                return new kktResp(status, result);
                            } else {
                                return new kktResp(202, { message: "Wrong user Password!" });
                            }
                        } else {
                            return new kktResp(203, { message: "User is not group of this Application" });
                        }
                    } else {
                        console.log("NGGA ADA");
                        return new kktResp(203, { message: "No group Assigned to this user" });
                        // return new kktResp(status, {data:checkingP.data});
                    }
                } else {
                    return new kktResp(203, { message: `No User ${credential} founded!` });
                }
                //#################################################
                break;
            case 1:
                return new kktResp(203, { message: "LDAP Auth Not Set!" });;
                break;
            default:
                return new kktResp(203, { message: "No Login Type Identified" });
                break;
        }

    } catch (error) {
        console.log(error);
        return new kktResp(500, error);
    }
}

const signOut = async (req, res, next) => {
    try {
        // const { appname } = req.body;
        console.log("LOGOUT NIH");
        let tokenId = req.signedCookies['x-cookie']; console.log("LOGOUT NIH");
        if (tokenId) updateCookie = await pool("kkt_cookie_logs").update({ status_cookie: 2 }, ["status_cookie"]).where({ "id_cookie": tokenId });
        // user logged off
        // console.log(updateCookie);
        return new kktResp(200, { data: "user logged off" });
    } catch (error) {
        console.log(error);
        return new kktResp(500, error);
    }
}

const checkCookiesPage = async (req, res, next) => {
    try {
        // const { appname } = req.body;
        console.log("Check Halaman KKT Nih");
        let tokenId = req.signedCookies['x-cookie'];
        if (tokenId) {
            const cookiesRecord = await pool.select("kcl.*").from("kkt_cookie_logs as kcl").where({ "kcl.id_cookie": tokenId }).orderBy("kcl.created_at", "DESC");
            if (cookiesRecord.length > 0) {
                var result = cookiesRecord[0].res_meta;
                return new kktResp(200, result);
            } else {
                return new kktResp(204, "Non authorization session!");
            }
        } else {
            return new kktResp(203, "Non authorized page!");
        }
        // return new kktResp(200, {data:"user logged off"});
    } catch (error) {
        return new kktResp(500, error);
    }
}





// appname
module.exports = { signVia, signViaKeyApp, signOut, checkCookiesPage };