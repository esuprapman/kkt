const moment = require('moment-timezone');
const pool = require("../../../connection/db.js");
const jwttools = require("../../../utils/encryptdecryptjwt");
const kktResp = require("../../../models/kkt_resp");
const randomized = require("../../../utils/randomize");
const retriveUsersByCookie = async (req, res, next) => {
    console.log("####### RETRIVE Users BY Cookie ########");
    //############ Checking Aplication ID and Users
    let _userInfo = JSON.parse(req.userInfo.userinfo);
     const records = await pool.select("kmtu.*").from("kkt_mst_tenant_user as kmtu").where("kmtu.level_user",">",0).orderBy("kmtu.created_at", "DESC");
    let status = records.length > 0? 200 : 202;
        if(status == 200) {
            // let result = {status:status, data:allApps}
            let result = {records:records};
            if(req.userInfo) {
                result = {...result, _userInfo}
            }
            return new kktResp(status, result);
        } else {
            let result = {records:[]};
            if(req.userInfo) {
                result = {...result, _userInfo}
            }
            return new kktResp(status, result);
        }
}
const retriveUserByUserId = async (req, res, next) => {
    try {
        console.log("####### RETRIVE User BY APP ID ########");
        //############ Checking Aplication ID and Users
        const {id_user} = req.body;
        let _userInfo = JSON.parse(req.userInfo.userinfo);
        const allApps = await pool.select("kma.*").from("kkt_mst_tenant_user as kma").where("id_user", id_user);
        // console.log(allApps);
        let status = allApps.length > 0? 200 : 202;
        if(status == 200) {
            let result = {records:allApps};
            if(req.userInfo) {
                result = {...result, _userInfo}
            }
            return new kktResp(status, result);
        } else {
            let result = {records:[]};
            if(req.userInfo) {
                result = {...result, _userInfo}
            }
            return new kktResp(status, result);
        }
    } catch (error) {
        return new kktResp(500, error);
    }
}
const retriveUserNotGrouped = async (req, res, next) => {
    try {
        let _userInfo = JSON.parse(req.userInfo.userinfo);
        console.log("####### RETRIVE User BY NO GROUP ########");
            const allUsers = await pool.select("kmtu.*", "ksgu.id_set_group").from("kkt_mst_tenant_user as kmtu").leftOuterJoin("kkt_set_group_user as ksgu", "kmtu.id_user", "ksgu.id_user_set_group").where("kmtu.level_user",">",0).where("ksgu.id_set_group", null);
            // console.log(`Data `,allUsers);
            let status = allUsers.length > 0? 200 : 202;
            // let result = allUsers;
            let result = {records:allUsers};
            if(req.userInfo) {
                result = {...result, _userInfo}
            }
            return new kktResp(status, result);
      
    } catch (error) {
        console.log(error);
        return new kktResp(500, error);
    }
}



const updateUser = async (req, res, next) =>{
    console.log("##### UPDATE User!");
    // {"id_app":"","name_app":"BULK-D","desc_app":"Aplikasi Dashbioard BI Fast","id_tenant_app":"","created_by":"","use_approve_app":{"code":0,"name":"False"},"link_app":"http://localhost:4201","path_icon":"http://localhost:3000/assets/images1680187802818-917771009-SeqDiagramLinkAja.png","key_app":"","status":{"code":1,"name":"True"}}
     try {
         let _userInfo = JSON.parse(req.userInfo.userinfo);
        //  {"id_user":"10232465","id_tenant_user":"101231","keyid_user":"admingarada","name_user":"Administrator Garda Ada saja","email_user":"muktiryan@gmail.com","mobile_user":"(+62)8569-4463-308","level_user":{"code":1,"name":"Regular"},"secret_user":"70682896e24287b0476eff2a14c148f0"}
             const {id_user,keyid_user,name_user,email_user,mobile_user} = req.body;
             let currentDate = new Date();
             var updateRecord = await pool("kkt_mst_tenant_user").update({
                    keyid_user:keyid_user,
                    name_user:name_user,
                    email_user:email_user,
                    mobile:mobile_user,
                    updated_at:new Date()
                },
                 ["id_user", "name_user"]).where({"id_user":id_user});
                 console.log(updateRecord);
             if(updateRecord) {
                 return new kktResp(200, {status:200, data:updateRecord});
             } else {
                 return new kktResp(203, {status:203, data:{message: "Unable to Update Applications"}});
             }
     } catch (error) {
         return new kktResp(500, {status:500, data:{message: error}});
     }

}
const deleteUser = async (req, res, next) =>{
    console.log("##### DELETE User!");
    try {
        const {id_user} = req.body;
            var deleteRecord = await pool("kkt_mst_tenant_user").delete().where({"id_user":id_user});
            if(deleteRecord) {
                return new kktResp(200, {status:200, data:{message:'Delete User success!'}});
            } else {
                return new kktResp(203, {status:203, data:{message: "Unable to delete User"}});
            }
    } catch (error) {
        return new kktResp(500, {status:500, data:{message: error}});
    }
}
const addUser = async (req, res, next) =>{
    console.log("##### ADD User!");
    try {
        let _userInfo = JSON.parse(req.userInfo.userinfo);
        // console.log(req.userInfo.userinfo);
        // {"id_user":"10299999","id_tenant_user":"101231","id_ext_tenant_user":null,"keyid_user":"admin","name_user":"Administrator","login_type_user":0,"level_user":0,"status_user":1,"name_tenant":"PT Mitra Integrasi Informatika","desc_tenant":"Perusahaan Konsultan IT terbesar di Indonesia dan di Amerika","address_tenant":"APL Tower Lt 37122332","phone_tenant":"02124023324","email_tenant":"admin@mii.co.id"}
        const {id_user,id_tenant_user,keyid_user,name_user,email_user,mobile_user,level_user,secret_user} = req.body;
        // {"id_user":"","id_tenant_user":"","keyid_user":"upet","name_user":"Udin Petot","email_user":"muktiadhiryan@gmail.com","mobile_user":"(+62)8569-4463-308","level_user":{"code":1,"name":"Regular"},"secret_user":"70682896e24287b0476eff2a14c148f0"}
        let random = await randomized.randomByRange(10000,99999);
        let idUser = 10200000+random;
        // id_tenant_user
            var insertRecord = await pool("kkt_mst_tenant_user").insert({
                id_user:idUser,
                keyid_user:keyid_user,
                name_user:name_user,
                email_user:email_user,
                mobile:mobile_user,
                status_user:1,
                created_by:_userInfo.id_user,
                id_tenant_user:_userInfo.id_tenant_user,
                level_user:level_user.code,
                secret_user:secret_user
            },
                ["id_user", "name_user"]);
            if(insertRecord) {
                return new kktResp(200, {status:200, data:insertRecord});
            } else {
                return new kktResp(203, {status:203, data:{message: "Unable to add User"}});
            }
    } catch (error) {
        console.log(error);
        return new kktResp(500, {status:500, data:{message: error}});
    }

}




module.exports = { retriveUsersByCookie,addUser,retriveUserByUserId,updateUser,deleteUser,retriveUserNotGrouped };