const moment = require('moment-timezone');
const pool = require("../../../connection/db.js");
const kktResp = require("../../../models/kkt_resp");
const randomized = require("../../../utils/randomize");

const retriveModulesByCookie = async (req, res, next) => {
    console.log("####### RETRIVE Modules BY Cookie ########");
    //############ Checking Aplication ID and modules
    let _userInfo = JSON.parse(req.userInfo.userinfo);
    // console.log(`User Info Di General ${JSON.stringify(_userInfoTmp)}`);
    // {"id_user":"10212345","id_tenant_user":"101231","id_ext_tenant_user":null,"keyid_user":"contribkomi","name_user":"Contrib Admin","login_type_user":0,"level_user":1,"status_user":1,"name_tenant":"PT Mii","desc_tenant":"Perusahaan Konsultan IT terbesar di Indonesia","address_tenant":"APL Tower Lt 37","phone_tenant":"02124023324","email_tenant":"admin@mii.co.id"}
    // const records = await pool.select("kmam.*").from("kkt_mst_apps_modules as kmam").orderBy([{column:"kmam.id_app_module", order:"asc"},{column:"kmam.seq", order:"asc"}]);
    const records = await pool.select("kmam.*","kmap.name_app").from("kkt_mst_apps_modules as kmam").innerJoin("kkt_mst_apps as kmap","kmam.id_app_module","kmap.id_app").orderBy([{column:"kmam.id_app_module", order:"asc"},{column:"kmam.seq", order:"asc"}]);
    let status = records.length > 0? 200 : 202;
        if(status == 200) {
            // let result = {status:status, data:allApps}
            let result = {records:records};
            
            if(req.userInfo) {
                // let _userInfoTmp = JSON.parse(req.userInfo.userinfo);
                result = {...result, _userInfo}
            }
            // console.log(result);
            return new kktResp(status, result);
        } else {
            let result = {records:[]};
            if(req.userInfo) {
                // let _userInfoTmp = JSON.parse(req.userInfo.userinfo);
                result = {...result, _userInfo}
            }
            return new kktResp(status, result);
            // return new kktResp(status, {status:status, data:"No Modules found!"});
        }
}
const retriveModulesNoChild = async (req, res, next) => {
    console.log("####### RETRIVE Modules BY Cookie ########");
    //############ Checking Aplication ID and modules
    let _userInfo = JSON.parse(req.userInfo.userinfo);
    const records = await pool.select("kmam.*","kmap.name_app").from("kkt_mst_apps_modules as kmam").innerJoin("kkt_mst_apps as kmap","kmam.id_app_module","kmap.id_app").where({"children_menu":false, "children_module":false}).orderBy([{column:"kmam.id_app_module", order:"asc"},{column:"kmam.seq", order:"asc"}]);
    let status = records.length > 0? 200 : 202;
        if(status == 200) {
            // let result = {status:status, data:allApps}
            let result = {records:records};
            
            if(req.userInfo) {
                // let _userInfoTmp = JSON.parse(req.userInfo.userinfo);
                result = {...result, _userInfo}
            }
            // console.log(result);
            return new kktResp(status, result);
        } else {
            let result = {records:[]};
            if(req.userInfo) {
                // let _userInfoTmp = JSON.parse(req.userInfo.userinfo);
                result = {...result, _userInfo}
            }
            return new kktResp(status, result);
            // return new kktResp(status, {status:status, data:"No Modules found!"});
        }
}
const retriveModuleById = async (req, res, next) => {
    try {
        console.log("####### RETRIVE MODULE BY ID ########");
        //############ Checking Aplication ID and modules
        const {id_module} = req.body;
        let _userInfo = JSON.parse(req.userInfo.userinfo);
        const allApps = await pool.select("kma.*").from("kkt_mst_apps_modules as kma").where("id_module", id_module);
        console.log(allApps);
        let status = allApps.length > 0? 200 : 202;
        if(status == 200) {
            let result = {records:allApps};
            if(req.userInfo) {
                result = {...result, _userInfo}
            }
            return new kktResp(status, result);
        } else {
            let result = {records:[]};
            if(req.userInfo) {
                result = {...result, _userInfo}
            }
            return new kktResp(status, result);
        }
    } catch (error) {
        return new kktResp(500, error);
    }
}

const retriveModuleByAppId = async (req, res, next) => {
    try {
        // console.log("####### RETRIVE MODULE BY APP ID ########");
        //############ Checking Aplication ID and modules
        const {id_app} = req.body;
        let _userInfo = JSON.parse(req.userInfo.userinfo);
        const allApps = await pool.select("kma.*").from("kkt_mst_apps_modules as kma").where("id_app_module", id_app);
        // console.log(allApps);
        let status = allApps.length > 0? 200 : 202;
        if(status == 200) {
            let result = {records:allApps};
            if(req.userInfo) {
                result = {...result, _userInfo}
            }
            return new kktResp(status, result);
        } else {
            let result = {records:[]};
            if(req.userInfo) {
                result = {...result, _userInfo}
            }
            return new kktResp(status, result);
        }
    } catch (error) {
        return new kktResp(500, error);
    }
}
const updateModule = async (req, res, next) =>{
    console.log("##### UPDATE Module!");
    // {"id_app":"","name_app":"BULK-D","desc_app":"Aplikasi Dashbioard BI Fast","id_tenant_app":"","created_by":"","use_approve_app":{"code":0,"name":"False"},"link_app":"http://localhost:4201","path_icon":"http://localhost:3000/assets/images1680187802818-917771009-SeqDiagramLinkAja.png","key_app":"","status":{"code":1,"name":"True"}}
     try {
         let _userInfo = JSON.parse(req.userInfo.userinfo);
         // console.log(req.userInfo.userinfo);
        //  const {id_app,name_app,desc_app,id_tenant_app,created_by,use_approve_app,link_app,path_icon,key_app,status} = req.body;
         const {id_module,name_module,desc_module,id_app_module,children_menu,link_module,icon_module,status,created_by,seq,children_module,parent_module, name_app} = req.body;
         let currentDate = new Date();
             var updateRecord = await pool("kkt_mst_apps_modules").update({
                    name_module:name_module,
                    desc_module:desc_module,
                    id_app_module:id_app_module.code,
                    children_menu:children_menu.code==1?true:false,
                    link_module:link_module==''?'-': link_module,
                    icon_module:icon_module==''?'pi-cog':icon_module,
                    status:status.code,
                    created_by:_userInfo.id_user,
                    seq:9,
                    children_module:children_module.code==1?true:false,
                    parent_module:parent_module,
                    updated_at:new Date()
                },
                 ["id_module", "name_module"]).where({"id_module":id_module});
                //  console.log(updateRecord);
             if(updateRecord) {
                 return new kktResp(200, {status:200, data:updateRecord});
             } else {
                 return new kktResp(203, {status:203, data:{message: "Unable to Update Applications"}});
             }
     } catch (error) {
         return new kktResp(500, {status:500, data:{message: error}});
     }

}
const deleteModule = async (req, res, next) =>{
    console.log("##### DELETE MODULE!");
   
    try {
        const {id_module} = req.body;
            var deleteRecord = await pool("kkt_mst_apps_modules").delete().where({"id_module":id_module});
            if(deleteRecord) {
                return new kktResp(200, {status:200, data:{message:'Delete Module success!'}});
            } else {
                return new kktResp(203, {status:203, data:{message: "Unable to delete Module"}});
            }
    } catch (error) {
        return new kktResp(500, {status:500, data:{message: error}});
    }
}
const addModule = async (req, res, next) =>{
    console.log("##### ADD MODULE!");
    try {
        let _userInfo = JSON.parse(req.userInfo.userinfo);
        // console.log(req.userInfo.userinfo);
        // {"id_module":"","name_module":"Biller Management","desc_module":"Halaman untuk CRUD Biller","id_app_module":{"code":"100235","name":"GARDA"},"children_menu":{"code":1,"name":"True"},"link_module":"","icon_module":"","status":{"code":1,"name":"True"},"created_by":"","seq":"","children_module":{"code":0,"name":"False"},"parent_module":"","name_app":""}
        const {id_module,name_module,desc_module,id_app_module,children_menu,link_module,icon_module,status,created_by,seq,children_module,parent_module, name_app} = req.body;
        let random = await randomized.randomByRange(10000,99999);
        let idmodule = 10200000+random;
            var insertRecord = await pool("kkt_mst_apps_modules").insert({
                id_module:idmodule,
                name_module:name_module,
                desc_module:desc_module,
                id_app_module:id_app_module.code,
                children_menu:children_menu.code==1?true:false,
                link_module:link_module==''?'-': link_module,
                icon_module:icon_module==''?'pi-cog':icon_module,
                status:status.code,
                created_by:_userInfo.id_user,
                seq:9,
                children_module:children_module.code==1?true:false,
                parent_module:parent_module
            },
                ["id_module", "name_module"]);
            if(insertRecord) {
                return new kktResp(200, {status:200, data:insertRecord});
            } else {
                return new kktResp(203, {status:203, data:{message: "Unable to add Module"}});
            }
    } catch (error) {
        console.log(error);
        return new kktResp(500, {status:500, data:{message: error}});
    }

}



module.exports = { retriveModulesByCookie, retriveModulesNoChild,retriveModuleById,updateModule,deleteModule,addModule,retriveModuleByAppId };