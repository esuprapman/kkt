const moment = require('moment-timezone');
const format2 = "YYMMDDHHmmss";
const _period = "YMM";
const formatISO = "YYYY-MM-DDTHH:mm:ss";
const pool = require("../../../connection/db.js");
const jwttools = require("../../../utils/encryptdecryptjwt");
const kktResp = require("../../../models/kkt_resp");
const { checkPassword } = require('../../../utils/passwordutils.js');
const { all } = require('../../../routes/krakatoa/commons/commonroute.js');

const retriveAttrNoApp = async (req, res, next) => {
    // console.log("####### RETRIVE Att No App Name ########");
    // console.log("### IS UNDEFINED #### ", req.userInfo === undefined);
    let _userInfoTmp = req.userInfo === undefined ? { data: undefined } : JSON.parse(req.userInfo.userinfo);
    // console.log("### IS USER (commoncontroller) UNDEFINED #### ",_userInfoTmp);
    return req.userInfo === undefined ? new kktResp(203, undefined) : new kktResp(200, _userInfoTmp);
}
const retriveAttrByAppName = async (req, res, next) => {
    console.log("####### RETRIVE Att BY Name ########");
    let _userInfoTmp = JSON.parse(req.userInfo.userinfo);
    let _AppName = req.params.id;
    //############ Checking Aplication ID and modules 
    const recordsApp = await pool.select("kma.*").from("kkt_mst_apps as kma").where({ "kma.name_app": _AppName.toUpperCase() });
    let _appId = recordsApp[0].id_app;
    if (recordsApp.length > 0) {
        //########### Get Modules Id Level 0 ###################
        let levelUser = _userInfoTmp.level_user;
        let recordsModules = [];
        if (levelUser < 1) {
            recordsModules = await pool.select("kmm.id_module", "kmm.name_module", "kmm.id_module", "kmm.desc_module", "kmm.children_menu", "kmm.icon_module").from("kkt_mst_apps_modules as kmm").where("kmm.id_app_module", _appId).orderBy("kmm.seq", "ASC");
        } else {
            //Check Group dari User dulu
            // const recordGroup = await pool.select("gu.id_group_set_group","mg.name_group","mgd.id_module_gd","kmm.*").from("kkt_set_group_user as gu").innerJoin("kkt_mst_group as mg", "gu.id_group_set_group","mg.id_group").innerJoin("kkt_mst_group_detail as mgd", "mg.id_group","mgd.id_header_gd").innerJoin("kkt_mst_apps_modules as kmm", "mgd.id_module_gd","kmm.id_module").where({"gu.id_user_set_group": _userInfoTmp.id_user, "mg.id_app_group":_appId});
            recordsModules = await pool.select("kmm.id_module", "kmm.name_module", "kmm.id_module", "kmm.desc_module", "kmm.children_menu", "kmm.icon_module", "mgd.id_menus_gd", "mg.id_roles_group").from("kkt_set_group_user as gu").innerJoin("kkt_mst_group as mg", "gu.id_group_set_group", "mg.id_group").innerJoin("kkt_mst_group_detail as mgd", "mg.id_group", "mgd.id_header_gd").innerJoin("kkt_mst_apps_modules as kmm", "mgd.id_module_gd", "kmm.id_module").where({ "gu.id_user_set_group": _userInfoTmp.id_user, "mg.id_app_group": _appId });
        }
        if (recordsModules.length > 0) {
            if (levelUser < 1) {
                await new Promise((resolve, reject) => {
                    recordsModules.forEach(async (value, index, array) => {
                        let objModule = value;
                        if (objModule.children_menu) {
                            let menuChilds = await pool.select("kmn.id_menu", "kmn.name_menu", "kmn.icon_menu", "kmn.link_menu", "kmn.seq_menu").from("kkt_mst_apps_menus as kmn").where("kmn.id_module_menu", objModule.id_module).orderBy("kmn.seq_menu", "ASC");
                            let role = { "id_roles_group": "A000" };
                            objModule = { ...objModule, ...role, ...{ children: menuChilds } }
                            recordsModules[index] = objModule;
                        }
                        if (index === array.length - 1) resolve();
                    });
                });
            } else {
                await new Promise((resolve, reject) => {
                    recordsModules.forEach(async (value, index, array) => {
                        let objModule = value;
                        if (objModule.children_menu) {
                            var childList = JSON.parse("[" + objModule.id_menus_gd + "]");
                            console.log("ARRAYNYA ", childList);
                            let menuChilds = await pool.select("kmn.id_menu", "kmn.name_menu", "kmn.icon_menu", "kmn.link_menu", "kmn.seq_menu").from("kkt_mst_apps_menus as kmn").whereIn("kmn.id_menu", childList).orderBy("kmn.seq_menu", "ASC");
                            objModule = { ...objModule, ...{ children: menuChilds } }
                            recordsModules[index] = objModule;
                        }
                        if (index === array.length - 1) resolve();
                    });
                });
            }
            // console.log("KELUAR DULUAN");
            let result = { userInfo: _userInfoTmp, isLoggedIn: true, appId: _appId, appName: _AppName, modules: recordsModules };
            let balikan = new kktResp(200, result)
            return balikan;
        } else {
            // console.log("KELUAR Tanpa Module");
            let result = { userInfo: _userInfoTmp, isLoggedIn: true, appId: _appId, appName: _AppName };
            let balikan = new kktResp(200, result)
            return balikan;
        }
    } else {
        let result = "No applications detected!";
        return new kktResp(203, result);
    }
}
const retriveAttrByKeyApp = async (req, res, next) => {
    console.log("####### RETRIVE Att BY Key APP ########");
    let _userInfoTmp = JSON.parse(req.userInfo.userinfo);
    let _AppName = req.params.id;
    console.log(`APP KEY ${_AppName}`);
    // console.log(`USER INFO ${req.userInfo.userinfo.id_app_group}`);
    //############ Checking Aplication ID and modules 
    const recordsApp = await pool.select("kma.*").from("kkt_mst_apps as kma").where({"kma.key_app": _AppName});
    // console.log(recordsApp);
    let _appId = undefined;

    if (recordsApp.length > 0) {
        //########### Get Modules Id Level 0 ###################
        _appId = recordsApp[0].id_app;
        console.log(`KODE KEY ${_appId}`);
        let levelUser = _userInfoTmp.level_user;
        let recordsModules = [];
        if (levelUser < 1) {
            recordsModules = await pool.select("kmm.id_module", "kmm.name_module", "kmm.id_module", "kmm.desc_module", "kmm.children_menu", "kmm.icon_module").from("kkt_mst_apps_modules as kmm").where("kmm.id_app_module", _appId).orderBy("kmm.seq", "ASC");
        } else {
            //Check Group dari User dulu
            recordsModules = await pool.select("kmm.id_module", "kmm.name_module", "kmm.id_module", "kmm.desc_module", "kmm.children_menu", "kmm.icon_module", "mgd.id_menus_gd", "mg.id_roles_group").from("kkt_set_group_user as gu").innerJoin("kkt_mst_group as mg", "gu.id_group_set_group", "mg.id_group").innerJoin("kkt_mst_group_detail as mgd", "mg.id_group", "mgd.id_header_gd").innerJoin("kkt_mst_apps_modules as kmm", "mgd.id_module_gd", "kmm.id_module").where({ "gu.id_user_set_group": _userInfoTmp.id_user, "mg.id_app_group": _appId });
        }
        if (recordsModules.length > 0) {
            if (levelUser < 1) {
                await new Promise((resolve, reject) => {
                    recordsModules.forEach(async (value, index, array) => {
                        let objModule = value;
                        if (objModule.children_menu) {
                            let menuChilds = await pool.select("kmn.id_menu", "kmn.name_menu", "kmn.icon_menu", "kmn.link_menu", "kmn.seq_menu").from("kkt_mst_apps_menus as kmn").where("kmn.id_module_menu", objModule.id_module).orderBy("kmn.seq_menu", "ASC");
                            let role = { "id_roles_group": "A000" };
                            objModule = { ...objModule, ...role, ...{ children: menuChilds } }
                            recordsModules[index] = objModule;
                        }
                        if (index === array.length - 1) resolve();
                    });
                });
            } else {
                await new Promise((resolve, reject) => {
                    recordsModules.forEach(async (value, index, array) => {
                        let objModule = value;
                        if (objModule.children_menu) {
                            var childList = JSON.parse("[" + objModule.id_menus_gd + "]");
                            // console.log("ARRAYNYA ", childList);
                            let menuChilds = await pool.select("kmn.id_menu", "kmn.name_menu", "kmn.icon_menu", "kmn.link_menu", "kmn.seq_menu").from("kkt_mst_apps_menus as kmn").whereIn("kmn.id_menu", childList).orderBy("kmn.seq_menu", "ASC");
                            objModule = { ...objModule, ...{ children: menuChilds } }
                            recordsModules[index] = objModule;
                        }
                        if (index === array.length - 1) resolve();
                    });
                });
            }
            console.log("KELUAR DULUAN");
            let result = { ..._userInfoTmp, ...{ isLoggedIn: true }, ...{ appId: _appId }, ...{ appName: _AppName }, ...{ modules: recordsModules } };
            // console.log("RESULT ADALAH ", JSON.stringify(result));

            let balikan = new kktResp(200, result)
            return balikan;
        } else {
            // console.log("KELUAR Tanpa Module");
            let result = { userInfo: _userInfoTmp, isLoggedIn: true, appId: _appId, appName: _AppName };
            let balikan = new kktResp(200, result)
            return balikan;
        }
    } else {
        let result = "No applications detected!";
        return new kktResp(203, result);
    }
}
const retrivePageByKeyApp = async (req, res, next) => {
    console.log("####### RETRIVE Att BY Key APP ########");
    const {keyapp, route} = req.body; 
    let _userInfoTmp = JSON.parse(req.userInfo.userinfo);

    let _AppName = req.params.id;
    console.log(`APP KEY ${_AppName}`);
    // console.log(`USER INFO ${req.userInfo.userinfo.id_app_group}`);
    //############ Checking Aplication ID and modules 
    const recordsApp = await pool.select("kma.*").from("kkt_mst_apps as kma").where({"kma.key_app": _AppName});
    console.log(recordsApp);
    let _appId = undefined;
    
    if(recordsApp.length > 0) {
        //########### Get Modules Id Level 0 ###################
        _appId= recordsApp[0].id_app;
        let levelUser = _userInfoTmp.level_user;
        let recordsModules = [];
        if(levelUser < 1) {
            let result = {..._userInfoTmp,...{isLoggedIn:true}, ...{appId:_appId}, ...{appName: _AppName}};
            let balikan = new kktResp(200,result);
            return balikan;
        } else {
            //Check Group dari User dulu
            console.log('USER INFO ', JSON.stringify(_userInfoTmp));







        }
    } else {
        let result = "No applications detected!";
        return new kktResp(203, result);
    }    
}

const retriveServiceByKeyApp = async (req, res, next) => {
    console.log("####### RETRIVE Att BY Key APP ########");
    const {keyapp, endpoint} = req.body; 
    let _userInfoTmp = JSON.parse(req.userInfo.userinfo);

    let _AppName = keyapp;
    // console.log(`APP KEY ${_AppName}`);
    // console.log(`USER INFO ${JSON.stringify(_userInfoTmp)}`);
    //############ Checking Aplication ID and modules 
    const recordsApp = await pool.select("kma.*").from("kkt_mst_apps as kma").where({"kma.key_app": _AppName});
    // console.log(recordsApp);
    let _appId = undefined;
    let _appNameDb = undefined;
    if(recordsApp.length > 0) {
        //########### Get Modules Id Level 0 ###################
        _appId= recordsApp[0].id_app;
        _appNameDb = recordsApp[0].name_app;
        let levelUser = _userInfoTmp.level_user;
        let _idGroup =  _userInfoTmp.id_group_set_group
        let recordsModules = [];
        if(levelUser < 1) {
            let result = {..._userInfoTmp,...{isLoggedIn:true}, ...{appId:_appId}, ...{appName: _AppName}};
            let balikan = new kktResp(200,result);
            return balikan;
        } else {
            //Check Group dari User dulu
            // console.log('USER INFO ', JSON.stringify(_userInfoTmp));
            console.log("ID APP ",_appId);
            console.log("ID GROUP ID ",_userInfoTmp.id_group_set_group);
            let getServiceRecordByIdApps = await pool.select("ksa.*").from("kkt_set_apis as ksa").where({"ksa.id_app_api":_appId, "ksa.endpoint_api":endpoint});
            if(getServiceRecordByIdApps.length > 0) {
                console.log("Service nya punya Nih");
                let stringRole = getServiceRecordByIdApps[0].id_roles_api;
                let stringRoleinUSER = "";
                let getGroupRoles = await pool.select("kmg.*").from("kkt_mst_group as kmg").where({"kmg.id_group":_idGroup});

                if(getGroupRoles.length > 0) {

                    let grpItem = getGroupRoles[0]; 
                    stringRoleinUSER = grpItem.id_roles_group
                    console.log(`ROLES GROUP ${stringRole}`);
                    console.log(`ROLES USERS ${stringRoleinUSER}`);
                     let isContain = false;
                     if(stringRole.length > stringRoleinUSER.length) {
                            console.log("User nya lebih sedikit");
                            if(stringRole.includes(stringRoleinUSER)) {
                                let result = {..._userInfoTmp,...{isLoggedIn:true}, ...{appId:_appId}, ...{appName: _appNameDb}};
                                let balikan = new kktResp(200,result);
                                return balikan;
                            } else {
                                let balikan = new kktResp(203,{message: "Not a group assignend to users"});
                                 return balikan;
                            }
                     
                     } else {
                        console.log("User nya lebih banyak atau sama");
                        if(stringRoleinUSER.includes(stringRole)) {
                            let result = {..._userInfoTmp,...{isLoggedIn:true}, ...{appId:_appId}, ...{appName: _appNameDb}};
                            let balikan = new kktResp(200,result);
                            return balikan;
                        } else {
                            let balikan = new kktResp(203,{message: "Not a group assignend to users"});
                             return balikan;
                        }
                     }
                    // let result = {..._userInfoTmp,...{isLoggedIn:true}, ...{appId:_appId}, ...{appName: _appNameDb}};
                    // let balikan = new kktResp(200,result);
                    // return balikan;
                } else {
                    let balikan = new kktResp(203,{message: "Not a group assignend to users"});
                    return balikan;
                }
            } else {
                console.log("Service ngga punya nih");
                // let result = {..._userInfoTmp,...{isLoggedIn:true}, ...{appId:_appId}, ...{appName: _appNameDb}};
                let balikan = new kktResp(203,{message: "Not authorized user for using this service"});
                return balikan;
            }
        }
    } else {
        let result = "No applications detected!";
        return new kktResp(203, result);
    }    
}



const retriveAllApps = async (req, res, next) => {
    try {
        // console.log("####### RETRIVE ALL APPS ########");
        const allApps = await pool.select("kma.*").from("kkt_mst_apps as kma").whereNotIn("created_by", ['SYSTEM']).orderBy("kma.name_app", "ASC");
        let status = allApps.length > 0 ? 200 : 202;
        if (status == 200) {
            let result = { status: status, data: allApps }
            if (req.userInfo) {
                let _userInfoTmp = JSON.parse(req.userInfo.userinfo);
                result = { ...result, ...{ userInfo: _userInfoTmp } }
            }
            // console.log(result);
            return new kktResp(status, { result });
        } else {
            return new kktResp(status, { status: status, data: "No applications found!" });
        }
    } catch (error) {
        return new kktResp(500, error);
    }
}
const retriveAppByName = async (req, res, next) => {
    console.log("####### RETRIVE APP BY Name ########");
}

const retriveRoles = async (req, res, next) => {
    try {
        // console.log("####### RETRIVE ALL APPS ########");
        const allApps = await pool.select("kmr.*").from("kkt_mst_roles as kmr").whereNotIn("id_role", ['A000']).orderBy("kmr.name_role", "ASC");
        let status = allApps.length > 0 ? 200 : 202;
        if (status == 200) {
            // let result = {status:status, data:allApps}
            let result = { records: allApps };
            if (req.userInfo) {
                let _userInfoTmp = JSON.parse(req.userInfo.userinfo);
                result = { ...result, ...{ userInfo: _userInfoTmp } }
            }
            return new kktResp(200, result);
        } else {
            let result = "No Roles detected!";
            return new kktResp(203, result);
        }
    } catch (error) {
        return new kktResp(500, error);
    }
}
const retriveBranchByAppObject = async (req, res, next) => {
    try {
        console.log("####### RETRIVE BRANCH APPS ########");
        const { code, name } = req.body;
        console.log(code);
        const allApps = await pool.select("kmb.*").from("kkt_mst_branch as kmb").where("kmb.id_app_branch", code).orderBy("kmb.id_branch", "ASC");
        console.log(allApps);
        let status = allApps.length > 0 ? 200 : 202;
        if (status == 200) {
            // let result = {status:status, data:allApps}
            let result = { records: allApps };
            if (req.userInfo) {
                let _userInfoTmp = JSON.parse(req.userInfo.userinfo);
                result = { ...result, ...{ userInfo: _userInfoTmp } }
            }
            // console.log(result);

            return new kktResp(200, result);
        } else {
            let result = "No Branch detected!";
            return new kktResp(203, result);
        }
    } catch (error) {
        return new kktResp(500, error);
    }
}


/////////////////////////////DIBUANG////////////////////////////////////////


const postProfit = async (req, res,next) =>{
    try {
        const {product,code,qty,istock,selisih,iddist} = req.body
        console.log("####### POST Profit ########");
        // console.log(`REQ ${product}`);
        var insertRecord = await pool("ETL_PROFIT_ROW").insert({
            CODE:code,
            PRODUCT:product,
            STORE:iddist,
            SELL:qty,
            STOCK:istock,
            SPARET:selisih,
        },
        ["CODE"]);
        let result = "Success Insert Data!";
        return new kktResp(200, result);

    } catch (error) {
        console.log(error);
        return new kktResp(500, error);
    }
    
}



module.exports = { retriveAllApps, retriveAppByName, retriveAttrByAppName, retriveAttrNoApp,retriveRoles,retriveBranchByAppObject,retriveAttrByKeyApp, retriveServiceByKeyApp, postProfit,retrivePageByKeyApp };
