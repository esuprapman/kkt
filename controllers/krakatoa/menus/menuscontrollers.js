const moment = require('moment-timezone');
const pool = require("../../../connection/db.js");

const kktResp = require("../../../models/kkt_resp");

const randomized = require("../../../utils/randomize");

const retriveMenusByCookie = async (req, res, next) => {
    console.log("####### RETRIVE Menus BY Cookie ########");
    //############ Checking Aplication ID and Menus
    let _userInfo = JSON.parse(req.userInfo.userinfo);
    
    // console.log(`User Info Di General ${JSON.stringify(_userInfoTmp)}`);

    const records = await pool.select("kman.*", "kmam.name_module", "kma.name_app").from("kkt_mst_apps_menus as kman").innerJoin("kkt_mst_apps_modules as kmam","kman.id_module_menu","kmam.id_module").innerJoin("kkt_mst_apps as kma","kman.id_app_menu","kma.id_app").orderBy("kman.seq_menu", "ASC");
    let status = records.length > 0? 200 : 202;
        if(status == 200) {
            // let result = {status:status, data:allApps}
            let result = {records:records};
            
            if(req.userInfo) {
                // let _userInfoTmp = JSON.parse(req.userInfo.userinfo);
                result = {...result, _userInfo}
            }
            // console.log(result);
            return new kktResp(status, result);
        } else {
            let result = {records:[]};
            if(req.userInfo) {
                // let _userInfoTmp = JSON.parse(req.userInfo.userinfo);
                result = {...result, _userInfo}
            }
            return new kktResp(status, result);
            // return new kktResp(status, {status:status, data:"No Menus found!"});
        }
}

const retriveMenuById = async (req, res, next) => {
    try {
        console.log("####### RETRIVE Menu BY ID ########");
        //############ Checking Aplication ID and Menus
        const {id_menu} = req.body;
        let _userInfo = JSON.parse(req.userInfo.userinfo);
        const allApps = await pool.select("kma.*").from("kkt_mst_apps_menus as kma").where("id_menu", id_menu);
        console.log(allApps);
        let status = allApps.length > 0? 200 : 202;
        if(status == 200) {
            let result = {records:allApps};
            if(req.userInfo) {
                result = {...result, _userInfo}
            }
            return new kktResp(status, result);
        } else {
            let result = {records:[]};
            if(req.userInfo) {
                result = {...result, _userInfo}
            }
            return new kktResp(status, result);
        }
    } catch (error) {
        return new kktResp(500, error);
    }
}
const retriveMenuByModuleId = async (req, res, next) => {
    try {
        console.log("####### RETRIVE Menu BY MODULE ID ########");
        //############ Checking Aplication ID and Menus
        const {id_module, menus} = req.body;
        let _userInfo = JSON.parse(req.userInfo.userinfo);
        let mnuTmp = [];
        //########## Not In#################
        await Promise.all(
            menus.map(async element2 =>{
                mnuTmp.push(element2.id_menu);
            })
        )

        console.log(`Ambil MENUNYA ${mnuTmp}`);

        const allApps = await pool.select("kma.*").from("kkt_mst_apps_menus as kma").where("id_module_menu", id_module).whereNotIn("id_menu",mnuTmp);
        // console.log(allApps);
        let status = allApps.length > 0? 200 : 202;
        if(status == 200) {
            let result = {records:allApps};
            if(req.userInfo) {
                result = {...result, _userInfo}
            }
            return new kktResp(status, result);
        } else {
            let result = {records:[]};
            if(req.userInfo) {
                result = {...result, _userInfo}
            }
            return new kktResp(status, result);
        }
    } catch (error) {
        return new kktResp(500, error);
    }
}



const updateMenu = async (req, res, next) =>{
    console.log("##### UPDATE Menu!");
    // {"id_app":"","name_app":"BULK-D","desc_app":"Aplikasi Dashbioard BI Fast","id_tenant_app":"","created_by":"","use_approve_app":{"code":0,"name":"False"},"link_app":"http://localhost:4201","path_icon":"http://localhost:3000/assets/images1680187802818-917771009-SeqDiagramLinkAja.png","key_app":"","status":{"code":1,"name":"True"}}
     try {
         let _userInfo = JSON.parse(req.userInfo.userinfo);
       
         const {id_menu,name_menu,desc_menu,id_app_menu,id_module_menu,link_menu,icon_menu,created_by,seq_menu} = req.body;
         let currentDate = new Date();
             var updateRecord = await pool("kkt_mst_apps_menus").update({
                    name_menu:name_menu,
                    desc_menu:desc_menu,
                    id_app_menu:id_app_menu.code,
                    link_menu:link_menu==''?'-': link_menu,
                    icon_menu:icon_menu==''?'pi-folder':icon_menu,
                    id_module_menu:id_module_menu.code,
                    updated_at:new Date()
                },
                 ["id_menu", "name_menu"]).where({"id_menu":id_menu});
                //  console.log(updateRecord);
             if(updateRecord) {
                 return new kktResp(200, {status:200, data:updateRecord});
             } else {
                 return new kktResp(203, {status:203, data:{message: "Unable to Update Applications"}});
             }
     } catch (error) {
         return new kktResp(500, {status:500, data:{message: error}});
     }

}
const deleteMenu = async (req, res, next) =>{
    console.log("##### DELETE Menu!");
   
    try {
        const {id_menu} = req.body;
            var deleteRecord = await pool("kkt_mst_apps_menus").delete().where({"id_menu":id_menu});
            if(deleteRecord) {
                return new kktResp(200, {status:200, data:{message:'Delete Menu success!'}});
            } else {
                return new kktResp(203, {status:203, data:{message: "Unable to delete Menu"}});
            }
    } catch (error) {
        return new kktResp(500, {status:500, data:{message: error}});
    }
}
const addMenu = async (req, res, next) =>{
    console.log("##### ADD Menu!");
    try {
        let _userInfo = JSON.parse(req.userInfo.userinfo);
        // console.log(req.userInfo.userinfo);
        // {"id_menu":"","name_menu":"Testing Menu","desc_menu":"Deskripsi menu nya","id_app_menu":{"code":"100123","name":"KOMI"},"id_module_menu":{"code":"10245632","name":"Settings"},"link_menu":"/setting/testingmenu","icon_menu":"","created_by":"","seq_menu":""}
        const {id_menu,name_menu,desc_menu,id_app_menu,id_module_menu,link_menu,icon_menu,created_by,seq_menu} = req.body;
        let random = await randomized.randomByRange(10000,99999);
        let idMenu = 10300000+random;
        // 10311144
        
            var insertRecord = await pool("kkt_mst_apps_menus").insert({
                id_menu:idMenu,
                name_menu:name_menu,
                desc_menu:desc_menu,
                id_app_menu:id_app_menu.code,
                link_menu:link_menu==''?'-': link_menu,
                icon_menu:icon_menu==''?'pi-folder':icon_menu,
                created_by:_userInfo.id_user,
                seq_menu:9,
                id_module_menu:id_module_menu.code
            },
                ["id_menu", "name_menu"]);
            if(insertRecord) {
                return new kktResp(200, {status:200, data:insertRecord});
            } else {
                return new kktResp(203, {status:203, data:{message: "Unable to add Menu"}});
            }
    } catch (error) {
        console.log(error);
        return new kktResp(500, {status:500, data:{message: error}});
    }

}




module.exports = { retriveMenusByCookie, retriveMenuById,addMenu,updateMenu,deleteMenu,retriveMenuByModuleId };