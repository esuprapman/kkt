const moment = require('moment-timezone');
const format2 = "YYMMDDHHmmss";
const _period = "YMM";
const formatISO = "YYYY-MM-DDTHH:mm:ss";
const pool = require("../../../connection/db.js");
const jwttools = require("../../../utils/encryptdecryptjwt");
const randomized = require("../../../utils/randomize");
const kktResp = require("../../../models/kkt_resp");
// const { checkPassword } = require('../../../utils/passwordutils.js');
// const { result } = require('lodash');
const md5 = require("md5");

const retriveAllApps = async (req, res, next) => {
    try {
        console.log("####### RETRIVE Applications BY Cookie ########");
        //############ Checking Aplication ID and modules
        let _userInfo = JSON.parse(req.userInfo.userinfo);
        // const modulesRec = await pool.select('mam.id_app_module').count('mam.id_module as modules').from("kkt_mst_apps_modules as mam").groupBy('mam.id_app_module').as("modRec");
        // console.log(JSON.stringify(modulesRec));
        // const menusRec = await pool.select('mnu.id_app_menu').count('mnu.id_menu as menus').from("kkt_mst_apps_menus as mnu").groupBy('mnu.id_app_menu').as("mnuRec");
        // console.log(JSON.stringify(menusRec));
        const groupRec = await pool.select('grp.id_app_group').count('grp.id_group as groups').from("kkt_mst_group as grp").groupBy('grp.id_app_group').as("grpRec");
        // console.log(JSON.stringify(groupRec));
        // const testApps = await pool.select("kma.*","modRec.*").from("kkt_mst_apps as kma").leftJoin(pool.select('mam.id_app_module').count('mam.id_module as modules').from("kkt_mst_apps_modules as mam").groupBy('mam.id_app_module').as("modRec"), "kma.id_app", "modRec.id_app_module");
        // .whereNotIn("kma.created_by", ['SYSTEM']).orderBy("kma.name_app", "ASC");
        // console.log(JSON.stringify(testApps));
        // const allApps = await pool.select("kma.*").from("kkt_mst_apps as kma").whereNotIn("created_by", ['SYSTEM']).orderBy("kma.name_app", "ASC");
        const allApps = await pool.select("kma.*", "modRec.*", "mnuRec.*", "grpRec.*").from("kkt_mst_apps as kma").leftJoin(pool.select('mam.id_app_module').count('mam.id_module as modules').from("kkt_mst_apps_modules as mam").groupBy('mam.id_app_module').as("modRec"), "kma.id_app", "modRec.id_app_module").leftJoin(pool.select('mnu.id_app_menu').count('mnu.id_menu as menus').from("kkt_mst_apps_menus as mnu").groupBy('mnu.id_app_menu').as("mnuRec"), "modRec.id_app_module", "mnuRec.id_app_menu").leftJoin(pool.select('grp.id_app_group').count('grp.id_group as groups').from("kkt_mst_group as grp").groupBy('grp.id_app_group').as("grpRec"), "modRec.id_app_module", "grpRec.id_app_group");
        // console.log(allApps);
        let status = allApps.length > 0 ? 200 : 202;
        if (status == 200) {
            let result = { records: allApps };
            if (req.userInfo) {
                result = { ...result, _userInfo }
            }
            return new kktResp(status, result);
        } else {
            let result = { records: [] };
            if (req.userInfo) {
                result = { ...result, _userInfo }
            }
            return new kktResp(status, result);
        }
    } catch (error) {
        console.log(error);
        return new kktResp(500, error);
    }
}
const retriveAppsById = async (req, res, next) => {
    try {
        console.log("####### RETRIVE Applications BY ID ########");
        //############ Checking Aplication ID and modules
        const { id_app } = req.body;
        let _userInfo = JSON.parse(req.userInfo.userinfo);
        const allApps = await pool.select("kma.*").from("kkt_mst_apps as kma").where("id_app", id_app).orderBy("kma.name_app", "ASC");
        let status = allApps.length > 0 ? 200 : 202;
        if (status == 200) {
            let result = { records: allApps };
            if (req.userInfo) {
                result = { ...result, _userInfo }
            }
            return new kktResp(status, result);
        } else {
            let result = { records: [] };
            if (req.userInfo) {
                result = { ...result, _userInfo }
            }
            return new kktResp(status, result);
        }
    } catch (error) {
        return new kktResp(500, error);
    }
}

const retriveAppsByKey = async (req, res, next) => {
    try {
        console.log("####### RETRIVE Applications BY Key ########");
        //############ Checking Aplication ID and modules
        const { keyid } = req.body;
        // let _userInfo = JSON.parse(req.userInfo.userinfo);
        const allApps = await pool.select("kma.*").from("kkt_mst_apps as kma").where("key_app", keyid).orderBy("kma.name_app", "ASC");
        let status = allApps.length > 0 ? 200 : 202;
        if (status == 200) {
            let result = { records: allApps };
            // if(req.userInfo) {
            //     result = {...result, _userInfo}
            // }
            return new kktResp(status, result);
        } else {
            let result = { records: [] };
            // if(req.userInfo) {
            //     result = {...result, _userInfo}
            // }
            return new kktResp(status, result);
        }
    } catch (error) {
        console.log(error);
        return new kktResp(500, error);
    }
}

const updatApplication = async (req, res, next) => {
    console.log("##### UPDATE APPLICation!");
    // {"id_app":"","name_app":"BULK-D","desc_app":"Aplikasi Dashbioard BI Fast","id_tenant_app":"","created_by":"","use_approve_app":{"code":0,"name":"False"},"link_app":"http://localhost:4201","path_icon":"http://localhost:3000/assets/images1680187802818-917771009-SeqDiagramLinkAja.png","key_app":"","status":{"code":1,"name":"True"}}
    try {
        let _userInfo = JSON.parse(req.userInfo.userinfo);
        // console.log(req.userInfo.userinfo);
        const { id_app, name_app, desc_app, id_tenant_app, created_by, use_approve_app, link_app, path_icon, key_app, status } = req.body;
        let currentDate = new Date();
        var updateRecord = await pool("kkt_mst_apps").update({
            name_app: name_app,
            desc_app: desc_app,
            id_tenant_app: _userInfo.id_tenant_user,
            use_approve_app: use_approve_app.code,
            link_app: link_app,
            path_icon: path_icon,
            status: status.code,
            updated_at: new Date()
        },
            ["id_app", "name_app"]).where({ "id_app": id_app });
        console.log(updateRecord);
        if (updateRecord) {
            return new kktResp(200, { status: 200, data: updateRecord });
        } else {
            return new kktResp(203, { status: 203, data: { message: "Unable to Update Applications" } });
        }
    } catch (error) {
        return new kktResp(500, { status: 500, data: { message: error } });
    }

}

const deleteApplication = async (req, res, next) => {
    console.log("##### DELETE APPLICATION!");

    try {
        const { id_app } = req.body;
        var deleteRecord = await pool("kkt_mst_apps").delete().where({ "id_app": id_app });
        if (deleteRecord) {
            return new kktResp(200, { status: 200, data: { message: 'Delete Application success!' } });
        } else {
            return new kktResp(203, { status: 203, data: { message: "Unable to delete Application" } });
        }
    } catch (error) {
        return new kktResp(500, { status: 500, data: { message: error } });
    }
}
const addApplication = async (req, res, next) => {
    console.log("##### ADD APPLICation!");
    // {"id_app":"","name_app":"BULK-D","desc_app":"Aplikasi Dashbioard BI Fast","id_tenant_app":"","created_by":"","use_approve_app":{"code":0,"name":"False"},"link_app":"http://localhost:4201","path_icon":"http://localhost:3000/assets/images1680187802818-917771009-SeqDiagramLinkAja.png","key_app":"","status":{"code":1,"name":"True"}}
    try {
        let _userInfo = JSON.parse(req.userInfo.userinfo);
        // console.log(req.userInfo.userinfo);
        const { id_app, name_app, desc_app, id_tenant_app, created_by, use_approve_app, link_app, path_icon, key_app, status } = req.body;
        let random = await randomized.randomByRange(100, 999);
        let idapp = 100000 + random;
        let keyapp = md5(idapp);


        let expApp = randomized.addDaysByDate("2023-12-30 00:56:00", 360);
        console.log(expApp);
        var insertRecord = await pool("kkt_mst_apps").insert({
            id_app: idapp,
            name_app: name_app,
            desc_app: desc_app,
            id_tenant_app: _userInfo.id_tenant_user,
            use_approve_app: use_approve_app.code,
            created_by: _userInfo.id_user,
            link_app: link_app,
            path_icon: path_icon,
            key_app: keyapp,
            status: status.code,
            expired_app: expApp
        },
            ["id_app", "name_app"]);
        // .where({"id_tenant":id_tenant});
        console.log("ANJAAAAAY");
        if (insertRecord) {
            return new kktResp(200, { status: 200, data: insertRecord });
        } else {
            return new kktResp(203, { status: 203, data: { message: "Unable to add Applications" } });
        }
    } catch (error) {
        console.log(error);
        return new kktResp(500, { status: 500, data: { message: error } });
    }

}



module.exports = { retriveAllApps, retriveAppsById, addApplication, updatApplication, deleteApplication, retriveAppsByKey };