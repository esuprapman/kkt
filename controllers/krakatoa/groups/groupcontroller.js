const moment = require('moment-timezone');
const pool = require("../../../connection/db.js");
const kktResp = require("../../../models/kkt_resp");
const randomized = require("../../../utils/randomize");

const retriveGroupsByCookie = async (req, res, next) => {
    console.log("####### RETRIVE Groups BY Cookie ########");
    //############ Checking Aplication ID and Groups
    let _userInfo = JSON.parse(req.userInfo.userinfo);
    // console.log(`User Info Di General ${JSON.stringify(_userInfoTmp)}`);
    // {"id_user":"10212345","id_tenant_user":"101231","id_ext_tenant_user":null,"keyid_user":"contribkomi","name_user":"Contrib Admin","login_type_user":0,"level_user":1,"status_user":1,"name_tenant":"PT Mii","desc_tenant":"Perusahaan Konsultan IT terbesar di Indonesia","address_tenant":"APL Tower Lt 37","phone_tenant":"02124023324","email_tenant":"admin@mii.co.id"}
    const records = await pool.select("kmg.*","kmr.name_role", "kma.name_app", "kmb.name_branch").from("kkt_mst_group as kmg").leftJoin("kkt_mst_roles as kmr","kmg.id_roles_group","kmr.id_role").innerJoin("kkt_mst_apps as kma","kmg.id_app_group","kma.id_app").leftJoin("kkt_mst_branch as kmb","kmg.id_branch_group","kmb.id_branch").orderBy("kmg.created_at", "DESC");
    let status = records.length > 0? 200 : 202;
        if(status == 200) {
            // let result = {status:status, data:allApps}
            let result = {records:records};
            if(req.userInfo) {
                // let _userInfoTmp = JSON.parse(req.userInfo.userinfo);
                result = {...result, _userInfo}
            }
            // console.log(result);
            return new kktResp(status, result);
        } else {
            let result = {records:[]};
            if(req.userInfo) {
                // let _userInfoTmp = JSON.parse(req.userInfo.userinfo);
                result = {...result, _userInfo}
            }
            return new kktResp(status, result);
            // return new kktResp(status, {status:status, data:"No Groups found!"});
        }
}
const retriveGroupById = async (req, res, next) => {
    try {
        console.log("####### RETRIVE Group BY APP ID ########");
        //############ Checking Aplication ID and Groups
        const {id_group} = req.body;
        let _userInfo = JSON.parse(req.userInfo.userinfo);
        const allApps = await pool.select("kma.*").from("kkt_mst_group as kma").where("id_group", id_group);
        console.log(allApps);
        let status = allApps.length > 0? 200 : 202;
        if(status == 200) {
            let result = {records:allApps};
            if(req.userInfo) {
                result = {...result, _userInfo}
            }
            return new kktResp(status, result);
        } else {
            let result = {records:[]};
            if(req.userInfo) {
                result = {...result, _userInfo}
            }
            return new kktResp(status, result);
        }
    } catch (error) {
        return new kktResp(500, error);
    }
}

const retriveGroupByIdMap = async (req, res, next) => {
    try {
        console.log("####### RETRIVE Group BY APP ID MAP ########");
        //############ Checking Aplication ID and Groups
        const {id_group} = req.body;
        let _userInfo = JSON.parse(req.userInfo.userinfo);
        const allGroup = await pool.select("kmg.*","kmr.name_role", "kma.name_app", "kmb.name_branch").from("kkt_mst_group as kmg").leftJoin("kkt_mst_roles as kmr","kmg.id_roles_group","kmr.id_role").innerJoin("kkt_mst_apps as kma","kmg.id_app_group","kma.id_app").leftJoin("kkt_mst_branch as kmb","kmg.id_branch_group","kmb.id_branch").where("id_group", id_group).orderBy("kmg.created_at","DESC");
      
        let status = allGroup.length > 0? 200 : 202;
        if(status == 200) {
            let grpObject = allGroup[0];
            let result = grpObject;
            //######## Get Modules connected #########
            
            console.log("####### RETRIVE Group BY APP CEK MAP ######## ",grpObject.id_group);
            const allGroupDetail = await pool.select("kgd.*","kmam.name_module","kmam.desc_module").from("kkt_mst_group_detail as kgd").innerJoin("kkt_mst_apps_modules as kmam","kgd.id_module_gd","kmam.id_module").where("kgd.id_header_gd", grpObject.id_group).orderBy("kgd.created_at","DESC");;
            
            if(allGroupDetail.length > 0){
                let moduleTmp = [];
                let modules = [];
                // console.log("TUNGGU LIAT MODULES");
                await allGroupDetail.map(async (element) =>{
                    let modTmp = element;
                    moduleTmp.push(modTmp);
                })
                // console.log(`MODULESNYA `, modules);
                if(moduleTmp.length > 0){
                    // console.log("TUNGGU LIAT MENU");
                    await Promise.all(
                        moduleTmp.map(async element2 =>{
                            var arrayMenu = JSON.parse("[" + element2.id_menus_gd + "]");
                            menus = await pool.select("kgm.*").from("kkt_mst_apps_menus as kgm").whereIn("id_menu", arrayMenu);
                            // console.log(element2);
                            element2 = {...element2, menus};
                            modules.push(element2);
                        })
                    )
                    result = {...result, modules};
                    // console.log(`RESULT `, result);
                    if(req.userInfo) {
                        result = {...result, _userInfo}
                    }
                    return new kktResp(status, result);
                } else {
                    result = {...result, modules};
                    console.log(`RESULT `, result);
                    if(req.userInfo) {
                        result = {...result, _userInfo}
                    }
                    return new kktResp(status, result);
                }



                // result = {...result, modules};
                // console.log(`RESULT `, result);
                // if(req.userInfo) {
                //     result = {...result, _userInfo}
                // }
                // return new kktResp(status, result);
            } else {
                console.log("NGGA MODULES");
                if(req.userInfo) {
                    result = {...result, _userInfo}
                }
                return new kktResp(status, result);
            }
           
        } else {
            let result = {records:[]};
            if(req.userInfo) {
                result = {...result, _userInfo}
            }
            return new kktResp(status, result);
        }
    } catch (error) {
        return new kktResp(500, error);
    }
}

const retriveGroupByIdUser = async (req, res, next) => {
    try {
        console.log("####### RETRIVE Group BY APP ID User ########");
        //############ Checking Aplication ID and Groups
        const {id_group} = req.body;
        let _userInfo = JSON.parse(req.userInfo.userinfo);
        const allGroupRecord = await pool.select("kmg.*","kmr.name_role", "kma.name_app", "kmb.name_branch").from("kkt_mst_group as kmg").leftJoin("kkt_mst_roles as kmr","kmg.id_roles_group","kmr.id_role").innerJoin("kkt_mst_apps as kma","kmg.id_app_group","kma.id_app").leftJoin("kkt_mst_branch as kmb","kmg.id_branch_group","kmb.id_branch").where("kmg.id_group", id_group).orderBy("kmg.created_at","DESC");
        console.log(allGroupRecord);
        let status = allGroupRecord.length > 0? 200 : 202;
        if(status == 200) {
            console.log("MASALAH 1");
            // let grpObject = allGroup[0];
            console.log("MASALAH 2");
            let result = allGroupRecord[0];
            //######## Get Modules connected #########
            
            console.log("####### RETRIVE Group BY USER CEK MAP ######## ",result);
            const allGroup = await pool.select("kg.*").from("kkt_mst_group as kg").where("kg.id_group", result.id_group).orderBy("kg.created_at","DESC");;
            
            if(allGroupRecord.length > 0){
                let grpObject = allGroupRecord[0];
                let result = grpObject;
                result = {...result, _userInfo};
                //*********************** Ambil data User************************* */
                // id_user,keyid_user,name_user,level_user,email_user,mobile
                const Users = await pool.select("ksgu.*","kmtu.id_user","kmtu.keyid_user","kmtu.name_user","kmtu.level_user","kmtu.email_user","kmtu.mobile").from("kkt_set_group_user as ksgu").innerJoin("kkt_mst_tenant_user as kmtu","ksgu.id_user_set_group","kmtu.id_user").where("ksgu.id_group_set_group", grpObject.id_group).orderBy("ksgu.created_at","DESC");
                // console.log(allUsers);
                if(Users.length > 0) {
                    // let usersmap = {users:allUsers}
                    result = {...result, Users};
                }
                return new kktResp(status, result);
            } else {
                console.log("NGGA MODULES");
                if(req.userInfo) {
                    result = {...result, _userInfo}
                }
                return new kktResp(status, result);
            }
           
        } else {
            let result = {records:[]};
            if(req.userInfo) {
                result = {...result, _userInfo}
            }
            return new kktResp(status, result);
        }
    } catch (error) {
        console.log(error);
        return new kktResp(500, error);
    }
}

const updateGroup = async (req, res, next) =>{
    console.log("##### UPDATE Group!");
    // {"id_app":"","name_app":"BULK-D","desc_app":"Aplikasi Dashbioard BI Fast","id_tenant_app":"","created_by":"","use_approve_app":{"code":0,"name":"False"},"link_app":"http://localhost:4201","path_icon":"http://localhost:3000/assets/images1680187802818-917771009-SeqDiagramLinkAja.png","key_app":"","status":{"code":1,"name":"True"}}
     try {
         let _userInfo = JSON.parse(req.userInfo.userinfo);
         // console.log(req.userInfo.userinfo);
        //  const {id_app,name_app,desc_app,id_tenant_app,created_by,use_approve_app,link_app,path_icon,key_app,status} = req.body;
        const {id_group,name_group,desc_group,id_app_group,id_roles_group,id_branch_group} = req.body;         
             var updateRecord = await pool("kkt_mst_group").update({
                    name_group:name_group,
                    desc_group:desc_group,
                    id_app_group:id_app_group.code,
                    id_roles_group:id_roles_group.code,
                    id_tenant_group:_userInfo.id_tenant_user,
                    created_by:_userInfo.id_user,
                    id_branch_group:id_branch_group.code,
                    updated_at:new Date()
                },
                 ["id_group", "name_group"]).where({"id_group":id_group});
                //  console.log(updateRecord);
             if(updateRecord) {
                 return new kktResp(200, {status:200, data:updateRecord});
             } else {
                 return new kktResp(203, {status:203, data:{message: "Unable to Update Group"}});
             }
     } catch (error) {
         return new kktResp(500, {status:500, data:{message: error}});
     }

}
const deleteGroup = async (req, res, next) =>{
    console.log("##### DELETE Group!");
   
    try {
        const {id_group} = req.body;
            var deleteRecord = await pool("kkt_mst_group").delete().where({"id_group":id_group});
            if(deleteRecord) {
                return new kktResp(200, {status:200, data:{message:'Delete Group success!'}});
            } else {
                return new kktResp(203, {status:203, data:{message: "Unable to delete Group"}});
            }
    } catch (error) {
        return new kktResp(500, {status:500, data:{message: error}});
    }
}
const deleteGroupMap = async (req, res, next) =>{
    console.log("##### DELETE Group Map!");
   
    try {
        const {id_seq_gd} = req.body;
            var deleteRecord = await pool("kkt_mst_group_detail").delete().where({"id_seq_gd":id_seq_gd});
            if(deleteRecord) {
                return new kktResp(200, {status:200, data:{message:'Delete Group Map success!'}});
            } else {
                return new kktResp(203, {status:203, data:{message: "Unable to delete Group Map"}});
            }
    } catch (error) {
        return new kktResp(500, {status:500, data:{message: error}});
    }
}
const deleteGroupMenu = async (req, res, next) =>{
    console.log("##### DELETE Group Menu!");
    // console.log(object);
    try {
        const {id_seq_gd,id_menu, menus} = req.body;
        let menuTemp=[]
        await Promise.all(
            menus.map(async element2 =>{
                if(element2.id_menu != id_menu) {
                    menuTemp.push(element2.id_menu);
                }
            })
        )
        var editMenugroup = await pool("kkt_mst_group_detail").update({
            id_menus_gd:menuTemp.toString(),
            updated_at:new Date()
        },
         ["id_seq_gd"]).where({"id_seq_gd":id_seq_gd});
        return new kktResp(200, editMenugroup);
    } catch (error) {
        return new kktResp(500, {status:500, data:{message: error}});
    }
}
const deleteGroupUser = async (req, res, next) =>{
    console.log("##### DELETE Group User!");
    // console.log(object);
    try {
        const {id_seq_gd} = req.body;
        console.log(JSON.stringify(req.body));
        var deleteUsergroup = await pool("kkt_set_group_user").delete().where({"id_set_group":id_seq_gd});
        return new kktResp(200, deleteUsergroup);
    } catch (error) {
        return new kktResp(500, {status:500, data:{message: error}});
    }
}


const addGroup = async (req, res, next) =>{
    console.log("##### ADD Group!");
    try {
        let _userInfo = JSON.parse(req.userInfo.userinfo);
        // console.log(req.userInfo.userinfo);
        // {"id_Group":"","name_Group":"Biller Management","desc_Group":"Halaman untuk CRUD Biller","id_app_Group":{"code":"100235","name":"GARDA"},"children_menu":{"code":1,"name":"True"},"link_Group":"","icon_Group":"","status":{"code":1,"name":"True"},"created_by":"","seq":"","children_Group":{"code":0,"name":"False"},"parent_Group":"","name_app":""}

        // {"id_group":"","name_group":"G_KOMI_CREATOR","desc_group":"Testing Group","id_app_group":{"code":"100123","name":"KOMI"},"id_roles_group":{"code":"A001","name":"Contributor"},"id_branch_group":{"code":"3300001","name":"default"},"created_by":""}


        const {id_group,name_group,desc_group,id_app_group,id_roles_group,id_branch_group} = req.body;
        let random = await randomized.randomByRange(10000,99999);
        let idGroup = 20000000+random;
            var insertRecord = await pool("kkt_mst_group").insert({
                id_group:idGroup,
                name_group:name_group,
                desc_group:desc_group,
                id_app_group:id_app_group.code,
                id_roles_group:id_roles_group.code,
                id_tenant_group:_userInfo.id_tenant_user,
                created_by:_userInfo.id_user,
                id_branch_group:id_branch_group.code
            },
                ["id_group", "name_group"]);
            if(insertRecord) {
                return new kktResp(200, {status:200, data:insertRecord});
            } else {
                return new kktResp(203, {status:203, data:{message: "Unable to add Group"}});
            }
    } catch (error) {
        console.log(error);
        return new kktResp(500, {status:500, data:{message: error}});
    }

}

const addGroupMap = async (req, res, next) =>{
    console.log("##### ADD Group!");
    try {
        let _userInfo = JSON.parse(req.userInfo.userinfo);
        // {"id_group":"20057456","id_module":"10245632","name_module":"Settings","desc_module":"Setting of Komi application","id_app_module":"100123","children_menu":true,"link_module":"-","icon_module":"pi-cog","status":1,"created_by":"10299999","created_at":"2023-03-24T12:36:33.524Z","updated_at":null,"seq":"1","children_module":false,"parent_module":null}
        const {id_group, id_module,name_module,desc_module,id_app_module,children_menu,link_module,icon_module,status} = req.body;
        let random = await randomized.randomByRange(10000,99999);
        let idSeqGroup = 201000000+random;
            var insertRecord = await pool("kkt_mst_group_detail").insert({
                id_seq_gd:idSeqGroup,
                id_header_gd:id_group,
                id_module_gd:id_module,
                created_by:_userInfo.id_user,
            },
                ["id_seq_gd", "id_header_gd"]);
            if(insertRecord) {
                return new kktResp(200, {status:200, data:insertRecord});
            } else {
                return new kktResp(203, {status:203, data:{message: "Unable to add Group"}});
            }
    } catch (error) {
        console.log(error);
        return new kktResp(500, {status:500, data:{message: error}});
    }

}

const addGroupMenu = async (req, res, next) =>{
    console.log("##### ADD Group MENU!");
    let result = {};
    try {
        let arrayTmp = [];
        let _userInfo = JSON.parse(req.userInfo.userinfo);
        const{module_obj, menu_add} = req.body
       
        console.log(`Module : ${JSON.stringify(module_obj)}`);
        // Module : {"id_seq_gd":"201087261","id_header_gd":"20076411","id_module_gd":"10223079","id_menus_gd":null,"created_by":"10299999","created_at":"2023-04-04T08:24:34.819Z","updated_at":null,"name_module":"Billing Master","desc_module":"Master Billing for Garda","menus":[]}
        if(module_obj.menus.length > 0){
            await Promise.all(
                module_obj.menus.map(async element2 =>{
                    arrayTmp.push(element2.id_menu);
                })
            )
        }
        console.log(`Menus : ${JSON.stringify(menu_add)}`);
        // Menus : [{"id_menu":"10367006","name_menu":"List Biller","desc_menu":"Daftar Mater biller","icon_menu":"pi-folder","link_menu":"/mst/listbiller","created_by":"10299999","created_at":"2023-04-01T14:17:40.564Z","updated_at":null,"id_module_menu":"10223079","seq_menu":"9","id_app_menu":"100235"}]
        if(menu_add.length > 0){
            await Promise.all(
                menu_add.map(async element2 =>{
                    arrayTmp.push(element2.id_menu);
                })
            )
        }
        if(arrayTmp.length > 0) {
            const updateMenu = await pool("kkt_mst_group_detail").update({
                id_menus_gd:arrayTmp.toString(),
                updated_at:new Date()
            },["id_seq_gd"]).where("id_seq_gd",module_obj.id_seq_gd);
            if(req.userInfo) {
                result = {...result, _userInfo}
            }
            arrayTmp={};
            return new kktResp(200, result);
        } else {
            return new kktResp(203, result);
        }
    } catch (error) {
        console.log(error);
        return new kktResp(500, {status:500, data:{message: error}});
    }

}
const addGroupUser = async (req, res, next) =>{
    console.log("##### ADD Group User!");
    let result = {};
    let arrayTmp = [];
    try {
        let _userInfo = JSON.parse(req.userInfo.userinfo);
        const{id_group, users} = req.body

        if (users.length>0) {
            await Promise.all(
                users.map(async element2 =>{
                    let random = await randomized.randomByRange(10000,99999);
                    let idSeqGroup = 80100000+random;
                    // "id_set_group


                    let objectInsert = {id_set_group:idSeqGroup,id_user_set_group:element2.id_user,id_group_set_group:id_group,created_by: _userInfo.id_user}
                    arrayTmp.push(objectInsert);
                })
            );
            console.log(`ARRAY ${JSON.stringify(arrayTmp)}`);
            const insertMenuGroup = await pool("kkt_set_group_user").insert(arrayTmp);


            if(insertMenuGroup) {
                return new kktResp(200, {status:200, data:insertMenuGroup});
            } else {
                return new kktResp(203, {status:203, data:{message: "Unable to add Group"}});
            }
            
            return new kktResp(200, result);
        } else {
            if(req.userInfo) {

                result = {...result, _userInfo}
            }
            return new kktResp(202, "No User inserted");
        }

    } catch (error) {
        console.log(error);
        return new kktResp(500, {status:500, data:{message: error}});
    }
}
module.exports = { retriveGroupsByCookie, addGroup, updateGroup,deleteGroup,retriveGroupById,retriveGroupByIdMap, addGroupMap,addGroupMenu,deleteGroupMap,deleteGroupMenu,retriveGroupByIdUser,addGroupUser,deleteGroupUser };