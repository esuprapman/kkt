const moment = require("moment-timezone");
const pool = require("../../../connection/db.js");
const randomized = require("../../../utils/randomize");
const kktResp = require("../../../models/kkt_resp");
const { checkPassword } = require("../../../utils/passwordutils.js");

const retriveApiManByCookie = async (req, res, next) => {
  console.log("####### RETRIVE ApiMan BY Cookie ########");
  //############ Checking Aplication ID and ApiMan
  let _userInfo = JSON.parse(req.userInfo.userinfo);
  // console.log(`User Info Di General ${JSON.stringify(_userInfoTmp)}`);
  // {"id_user":"10212345","id_tenant_user":"101231","id_ext_tenant_user":null,"keyid_user":"contribkomi","name_user":"Contrib Admin","login_type_user":0,"level_user":1,"status_user":1,"name_tenant":"PT Mii","desc_tenant":"Perusahaan Konsultan IT terbesar di Indonesia","address_tenant":"APL Tower Lt 37","phone_tenant":"02124023324","email_tenant":"admin@mii.co.id"}
  const records = await pool
    .select("ksa.*")
    .from("kkt_set_apis as ksa")
    .orderBy("ksa.created_at", "DESC");
  let status = records.length > 0 ? 200 : 202;
  if (status == 200) {
    // let result = {status:status, data:allApps}
    let result = { records: records };
    if (req.userInfo) {
      // let _userInfoTmp = JSON.parse(req.userInfo.userinfo);
      result = { ...result, _userInfo };
    }
    return new kktResp(status, result);
  } else {
    let result = { records: [] };
    if (req.userInfo) {
      // let _userInfoTmp = JSON.parse(req.userInfo.userinfo);
      result = { ...result, _userInfo };
    }
    return new kktResp(status, result);
  }
};

//############################# API COMMON ##############################################
const publicApiCommon = async (req, res, next) => {
  console.log("####### RETRIVE ApiMan Common ########");
  //############ Checking Aplication ID and ApiMan
  let _userInfo = JSON.parse(req.userInfo.userinfo);
  const records = await pool
    .select("ksa.*")
    .from("kkt_set_apis as ksa")
    .where({ "ksa.type_api": 1, category: "PUBLIC" })
    .orderBy("ksa.created_at", "ASC");
  let status = records.length > 0 ? 200 : 202;
  if (status == 200) {
    // let result = {status:status, data:allApps}
    let result = { records: records };
    if (req.userInfo) {
      // let _userInfoTmp = JSON.parse(req.userInfo.userinfo);
      result = { ...result, _userInfo };
    }
    return new kktResp(status, result);
  } else {
    let result = { records: [] };
    if (req.userInfo) {
      // let _userInfoTmp = JSON.parse(req.userInfo.userinfo);
      result = { ...result, _userInfo };
    }
    return new kktResp(status, result);
  }
};

const publicApiConsume = async (req, res, next) => {
  console.log("####### RETRIVE ApiMan Consume ########");
  //############ Checking Aplication ID and ApiMan
  let _userInfo = JSON.parse(req.userInfo.userinfo);
  const records = await pool
    .select("ksa.*")
    .from("kkt_set_apis as ksa")
    .where({ "ksa.type_api": 2 })
    .orderBy("ksa.category", "ASC");
  let status = records.length > 0 ? 200 : 202;
  if (status == 200) {
    // let result = {status:status, data:allApps}
    let result = { records: records };
    if (req.userInfo) {
      // let _userInfoTmp = JSON.parse(req.userInfo.userinfo);
      result = { ...result, _userInfo };
    }
    return new kktResp(status, result);
  } else {
    let result = { records: [] };
    if (req.userInfo) {
      // let _userInfoTmp = JSON.parse(req.userInfo.userinfo);
      result = { ...result, _userInfo };
    }
    return new kktResp(status, result);
  }
};

const publicApiExternal = async (req, res, next) => {
  console.log("####### RETRIVE ApiMan Consume ########");
  //############ Checking Aplication ID and ApiMan
  let _userInfo = JSON.parse(req.userInfo.userinfo);
  const { id_app_api } = req.body;
  console.log(`@@@@@@@@@@@@@@@@@@ ${id_app_api}`);
  const records = await pool
    .select("ksa.*")
    .from("kkt_set_apis as ksa")
    .where({ "ksa.type_api": 3, id_app_api: id_app_api })
    .orderBy("ksa.category", "ASC");
  let status = records.length > 0 ? 200 : 202;
  if (status == 200) {
    // let result = {status:status, data:allApps}
    let result = { records: records };
    if (req.userInfo) {
      // let _userInfoTmp = JSON.parse(req.userInfo.userinfo);
      result = { ...result, _userInfo };
    }
    return new kktResp(status, result);
  } else {
    let result = { records: [] };
    if (req.userInfo) {
      // let _userInfoTmp = JSON.parse(req.userInfo.userinfo);
      result = { ...result, _userInfo };
    }
    return new kktResp(status, result);
  }
};

const retriveApiById = async (req, res, next) => {
  try {
    console.log("####### RETRIVE Applications BY ID ########");
    //############ Checking Aplication ID and modules
    const { id_app } = req.body;
    let _userInfo = JSON.parse(req.userInfo.userinfo);
    const allApps = await pool
      .select("kma.*")
      .from("kkt_set_apis as kma")
      .where("id_app", id_app)
      .orderBy("kma.name_app", "ASC");
    let status = allApps.length > 0 ? 200 : 202;
    if (status == 200) {
      let result = { records: allApps };
      if (req.userInfo) {
        result = { ...result, _userInfo };
      }
      return new kktResp(status, result);
    } else {
      let result = { records: [] };
      if (req.userInfo) {
        result = { ...result, _userInfo };
      }
      return new kktResp(status, result);
    }
  } catch (error) {
    return new kktResp(500, error);
  }
};

const retriveApiByAPIId = async (req, res, next) => {
  try {
    console.log("####### RETRIVE Applications BY API ID ########");
    //############ Checking Aplication ID and modules
    console.log(req.body);
    const { id_api } = req.body;
    let _userInfo = JSON.parse(req.userInfo.userinfo);
    const allApps = await pool
      .select("*")
      .from("kkt_set_apis")
      .where({ id_api });
    let status = allApps.length > 0 ? 200 : 202;
    if (status == 200) {
      let result = { records: allApps };
      if (req.userInfo) {
        result = { ...result, _userInfo };
      }
      return new kktResp(status, result);
    } else {
      let result = { records: [] };
      if (req.userInfo) {
        result = { ...result, _userInfo };
      }
      return new kktResp(status, result);
    }
  } catch (error) {
    console.log(error);
    return new kktResp(500, error);
  }
};

const updateApi = async (req, res, next) => {
  console.log("##### UPDATE APPLICation!");
  // {"id_app":"","name_app":"BULK-D","desc_app":"Aplikasi Dashbioard BI Fast","id_tenant_app":"","created_by":"","use_approve_app":{"code":0,"name":"False"},"link_app":"http://localhost:4201","path_icon":"http://localhost:3000/assets/images1680187802818-917771009-SeqDiagramLinkAja.png","key_app":"","status":{"code":1,"name":"True"}}
  try {
    let _userInfo = JSON.parse(req.userInfo.userinfo);
    // console.log(req.userInfo.userinfo);
    let arrayRoleTmp = [];
    const {
      id_api,
      id_roles_api,
      endpoint_api,
      id_app_api,
      desc_api,
      method_api,
    } = req.body;
    await Promise.all(
      id_roles_api.map(async (element2) => {
        arrayRoleTmp.push(element2.code);
      })
    );
    console.log(req.body);
    var updateRecord = await pool("kkt_set_apis")
      .update(
        {
          endpoint_api: endpoint_api,
          id_app_api: id_app_api.code,
          id_roles_api: arrayRoleTmp.map((x) => x).join(", "),
          desc_api: desc_api,
          type_api: 3,
          method_api: method_api.code,
        },
        ["id_api"]
      )
      .where({ id_api: id_api });
    if (updateRecord) {
      return new kktResp(200, { status: 200, data: updateRecord });
    } else {
      return new kktResp(203, {
        status: 203,
        data: { message: "Unable to Update Applications" },
      });
    }
  } catch (error) {
    console.log(error);
    return new kktResp(500, { status: 500, data: { message: error } });
  }
};
const deleteApi = async (req, res, next) => {
  console.log("##### DELETE APPLICATION!");

  try {
    const { id_app } = req.body;
    var deleteRecord = await pool("kkt_set_apis")
      .delete()
      .where({ id_app: id_app });
    if (deleteRecord) {
      return new kktResp(200, {
        status: 200,
        data: { message: "Delete Application success!" },
      });
    } else {
      return new kktResp(203, {
        status: 203,
        data: { message: "Unable to delete Application" },
      });
    }
  } catch (error) {
    return new kktResp(500, { status: 500, data: { message: error } });
  }
};
const addApi = async (req, res, next) => {
  console.log("##### ADD API!");
  // {"id_api":"","endpoint_api":"/apa","id_app_api":{"code":"0000","name":"All Applications"},"id_tenant_api":"","id_roles_api":[{"code":"A001","name":"Contributor"}],"desc_api":"TEST","type_api":3,"method_api":[{"code":"GET","name":"GET"}],"created_by":""}
  try {
    let arrayRoleTmp = [];
    let _userInfo = JSON.parse(req.userInfo.userinfo);
    // console.log(req.userInfo.userinfo);
    const {
      id_api,
      endpoint_api,
      id_app_api,
      id_tenant_api,
      id_roles_api,
      desc_api,
      type_api,
      method_api,
      created_by,
    } = req.body;
    let random = await randomized.randomByRange(100000, 999999);

    let idapi = 701000000 + random;

    await Promise.all(
      id_roles_api.map(async (element2) => {
        arrayRoleTmp.push(element2.code);
      })
    );
    // let expApp = randomized.addDaysByDate("2023-12-30 00:56:00",360);
    // console.log(expApp);
    var insertRecord = await pool("kkt_set_apis").insert(
      {
        id_api: idapi,
        endpoint_api: endpoint_api,
        id_app_api: id_app_api.code,
        id_tenant_api: _userInfo.id_tenant_user,
        id_roles_api: arrayRoleTmp.map((x) => x).join(", "),
        created_by: _userInfo.id_user,
        desc_api: desc_api,
        type_api: 3,
        method_api: method_api.code,
        jsonreq: "{}",
        jsonresp: "{}",
      },
      ["id_api"]
    );
    // console.log("ANJAAAAAY");
    if (insertRecord) {
      return new kktResp(200, { status: 200, data: insertRecord });
    } else {
      return new kktResp(203, {
        status: 203,
        data: { message: "Unable to add Applications" },
      });
    }
  } catch (error) {
    console.log(error);
    return new kktResp(500, { status: 500, data: { message: error } });
  }
};

module.exports = {
  retriveApiManByCookie,
  publicApiCommon,
  publicApiConsume,
  retriveApiById,
  retriveApiByAPIId,
  addApi,
  updateApi,
  deleteApi,
  publicApiExternal,
};
