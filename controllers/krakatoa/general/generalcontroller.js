const moment = require('moment-timezone');
const format2 = "YYMMDDHHmmss";
const _period = "YMM";
const formatISO = "YYYY-MM-DDTHH:mm:ss";
const pool = require("../../../connection/db.js");
const jwttools = require("../../../utils/encryptdecryptjwt");
const kktResp = require("../../../models/kkt_resp");
const { checkPassword } = require('../../../utils/passwordutils.js');

const retriveTenantByCookie = async (req, res, next) => {
    console.log("####### RETRIVE Tenant BY Cookie ######## ",req.userInfo);
    //############ Checking Aplication ID and modules
    let _userInfo = req.userInfo== undefined?{}:JSON.parse(req.userInfo.userinfo);
    const recordsTN = await pool.select("kmt.*").from("kkt_mst_tenant as kmt").where({"kmt.id_tenant": _userInfo.id_tenant_user});
    // let _appId = recordsApp[0].id_app;
    if(recordsTN.length > 0) {
        let result = {tenant: recordsTN[0]};
        // _userInfoTmp={userInfo: _userInfoTmp};
        result = {...result, _userInfo}
        // console.log(JSON.stringify(result));
        return new kktResp(200,result);
    } else {
        
        let result = "No Tenant detected!";
        return new kktResp(203, result);
    }    
}


const updateTenant = async (req, res, next) =>{
    console.log("##### UPDATING TENNANT!");
    try {
        const {id_tenant,name_tenant,desc_tenant,address_tenant,phone_tenant,email_tenant,created_by,loginimgpath,headimgpath} = req.body;
            var updateRecord = await pool("kkt_mst_tenant").update({
                name_tenant:name_tenant,
                desc_tenant:desc_tenant,
                address_tenant:address_tenant,
                email_tenant:email_tenant,
                created_by:created_by,
                loginimgpath:loginimgpath,
                headimgpath:headimgpath},
                ["id_tenant", "name_tenant"]).where({"id_tenant":id_tenant});

            if(updateRecord) {
                return new kktResp(200, {status:200, data:updateRecord});
            } else {
                return new kktResp(203, {status:203, data:{message: "Unable to update Tenant"}});
            }
    } catch (error) {
        return new kktResp(500, {status:500, data:{message: error}});
    }
}
const retriveTenantNoCookie = async (req, res, next) => {
    //############ Checking Aplication ID and modules
    const recordsTN = await pool.select("kmt.*").from("kkt_mst_tenant as kmt");
    if(recordsTN.length > 0) {
        let result = {tenant: recordsTN[0]};
        result = {...result}
        return new kktResp(200,result);
    } else {
        let result = "No Tenant detected!";
        return new kktResp(203, result);
    }    
}
module.exports = { retriveTenantByCookie, retriveTenantNoCookie, updateTenant };