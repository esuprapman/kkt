const moment = require("moment-timezone");
const format2 = "YYMMDDHHmmss";
const _period = "YMM";
const formatISO = "YYYY-MM-DDTHH:mm:ss";
const pool = require("../../../connection/db.js");
const jwttools = require("../../../utils/encryptdecryptjwt");
const randomized = require("../../../utils/randomize");
const kktResp = require("../../../models/kkt_resp");
const { checkPassword } = require("../../../utils/passwordutils.js");

const retriveParametersByCookie = async (req, res, next) => {
  console.log("####### RETRIVE Parameters BY Cookie ########");
  //############ Checking Aplication ID and Parameters
  let _userInfo = JSON.parse(req.userInfo.userinfo);
  // console.log(`User Info Di General ${JSON.stringify(_userInfoTmp)}`);
  // {"id_user":"10212345","id_tenant_user":"101231","id_ext_tenant_user":null,"keyid_user":"contribkomi","name_user":"Contrib Admin","login_type_user":0,"level_user":1,"status_user":1,"name_tenant":"PT Mii","desc_tenant":"Perusahaan Konsultan IT terbesar di Indonesia","address_tenant":"APL Tower Lt 37","phone_tenant":"02124023324","email_tenant":"admin@mii.co.id"}
  const records = await pool
    .select("kmp.*")
    .from("kkt_mst_params as kmp")
    .orderBy("kmp.created_at", "DESC");
  let status = records.length > 0 ? 200 : 202;
  if (status == 200) {
    // let result = {status:status, data:allApps}
    let result = { records: records };

    if (req.userInfo) {
      // let _userInfoTmp = JSON.parse(req.userInfo.userinfo);
      result = { ...result, _userInfo };
    }
    // console.log(result);
    return new kktResp(status, result);
  } else {
    let result = { records: [] };
    if (req.userInfo) {
      // let _userInfoTmp = JSON.parse(req.userInfo.userinfo);
      result = { ...result, _userInfo };
    }
    return new kktResp(status, result);
    // return new kktResp(status, {status:status, data:"No Parameters found!"});
  }
};

const retriveParamByAppId = async (req, res, next) => {
  try {
    console.log("####### RETRIVE Applications BY API ID ########");
    //############ Checking Aplication ID and modules
    console.log(req.body);
    const { id_app_param } = req.body;
    let _userInfo = JSON.parse(req.userInfo.userinfo);
    const allApps = await pool
      .select("*")
      .from("kkt_mst_params")
      .where({ id_app_param });
    let status = allApps.length > 0 ? 200 : 202;
    if (status == 200) {
      let result = { records: allApps };
      if (req.userInfo) {
        result = { ...result, _userInfo };
      }
      return new kktResp(status, result);
    } else {
      let result = { records: [] };
      if (req.userInfo) {
        result = { ...result, _userInfo };
      }
      return new kktResp(status, result);
    }
  } catch (error) {
    console.log(error);
    return new kktResp(500, error);
  }
};

const retriveParamByParamId = async (req, res, next) => {
  try {
    console.log("####### RETRIVE Applications BY API ID ########");
    //############ Checking Aplication ID and modules
    console.log(req.body);
    const { id_param } = req.body;
    let _userInfo = JSON.parse(req.userInfo.userinfo);
    const allApps = await pool
      .select("*")
      .from("kkt_mst_params")
      .where({ id_param });
    let status = allApps.length > 0 ? 200 : 202;
    if (status == 200) {
      let result = { records: allApps };
      if (req.userInfo) {
        result = { ...result, _userInfo };
      }
      return new kktResp(status, result);
    } else {
      let result = { records: [] };
      if (req.userInfo) {
        result = { ...result, _userInfo };
      }
      return new kktResp(status, result);
    }
  } catch (error) {
    console.log(error);
    return new kktResp(500, error);
  }
};

const updateParam = async (req, res, next) => {
  console.log("##### UPDATE APPLICation!");
  // {"id_app":"","name_app":"BULK-D","desc_app":"Aplikasi Dashbioard BI Fast","id_tenant_app":"","created_by":"","use_approve_app":{"code":0,"name":"False"},"link_app":"http://localhost:4201","path_icon":"http://localhost:3000/assets/images1680187802818-917771009-SeqDiagramLinkAja.png","key_app":"","status":{"code":1,"name":"True"}}
  try {
    let _userInfo = JSON.parse(req.userInfo.userinfo);
    // console.log(req.userInfo.userinfo);

    const { category_param, name_param, value_param, desc_param, id_param } =
      req.body;

    console.log(req.body);
    var updateRecord = await pool("kkt_mst_params")
      .update(
        {
          category_param,
          name_param,
          value_param,
          desc_param,
        },
        ["id_param"]
      )
      .where({ id_param });
    if (updateRecord) {
      return new kktResp(200, { status: 200, data: updateRecord });
    } else {
      return new kktResp(203, {
        status: 203,
        data: { message: "Unable to Update Applications" },
      });
    }
  } catch (error) {
    console.log(error);
    return new kktResp(500, { status: 500, data: { message: error } });
  }
};
const deleteParam = async (req, res, next) => {
  console.log("##### DELETE APPLICATION!");

  try {
    const { id_param } = req.body;
    console.log(id_param);
    var deleteRecord = await pool("kkt_mst_params")
      .delete()
      .where({ id_param: id_param });
    if (deleteRecord) {
      return new kktResp(200, {
        status: 200,
        data: { message: "Delete Application success!" },
      });
    } else {
      return new kktResp(203, {
        status: 203,
        data: { message: "Unable to delete Application" },
      });
    }
  } catch (error) {
    return new kktResp(500, { status: 500, data: { message: error } });
  }
};
const addParameter = async (req, res, next) => {
  console.log("##### ADD API!");
  // {"id_api":"","endpoint_api":"/apa","id_app_api":{"code":"0000","name":"All Applications"},"id_tenant_api":"","id_roles_api":[{"code":"A001","name":"Contributor"}],"desc_api":"TEST","type_api":3,"method_api":[{"code":"GET","name":"GET"}],"created_by":""}
  try {
    let arrayRoleTmp = [];
    let _userInfo = JSON.parse(req.userInfo.userinfo);
    // console.log(req.userInfo.userinfo);
    const {
      category_param,
      name_param,
      value_param,
      desc_param,
      id_app_param,
    } = req.body;
    let random = await randomized.randomByRange(100000, 999999);

    let id_param = 701000000 + random;
    var insertRecord = await pool("kkt_mst_params").insert(
      {
        id_param,
        category_param,
        name_param,
        value_param,
        desc_param,
        id_app_param,
        id_tenant_param: _userInfo.id_tenant_user,
        created_by: _userInfo.id_user,
        created_at: pool.fn.now(),
        type_param: 3,
      },
      ["id_param"]
    );
    // console.log("ANJAAAAAY");
    if (insertRecord) {
      return new kktResp(200, { status: 200, data: insertRecord });
    } else {
      return new kktResp(203, {
        status: 203,
        data: { message: "Unable to add Applications" },
      });
    }
  } catch (error) {
    console.log(error);
    return new kktResp(500, { status: 500, data: { message: error } });
  }
};

module.exports = {
  retriveParametersByCookie,
  addParameter,
  updateParam,
  deleteParam,
  retriveParamByAppId,
  retriveParamByParamId,
};
