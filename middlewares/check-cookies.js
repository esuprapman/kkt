const HttpError = require("../models/http-error");
const pool = require("../connection/db");


module.exports = async (req, res, next) => {
  try {
    let isiCookie = null;
    isiCookie = req.signedCookies['x-cookie'];
    if(isiCookie) {
        // console.log(`Ada Cookienya ${req.params.id}`);
        const recordsCookie = await pool.select("*").from("kkt_cookie_logs").where({"id_cookie": isiCookie, "status_cookie":1});
        if(recordsCookie.length > 0) {
          let tempUserInfo = recordsCookie[0].res_meta;
          req.userInfo = {userinfo:tempUserInfo}
        }
        next();
    } else {
       next();
    }
  } catch (err) {
    console.log(err);
    const error = new HttpError("Authentication failed!", 403);
    return next(error);
  }
};
