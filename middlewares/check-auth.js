const HttpError = require("../models/http-error");
const pool = require("../connection/db");
const kktResp = require("../models/kkt_resp");

module.exports = async (req, res, next) => {
  try {
    let isiCookie = null;
    isiCookie = req.signedCookies['x-cookie'];
    if(isiCookie) {
        // console.log(`Check Auth, apakah ada cookie? ${isiCookie}`);
        const recordsCookie = await pool.select("id_user_cookie", "created_by", "status_cookie", "loggin_attempt", "created_at", "updated_at", "req_meta", "res_meta", "ip_cookie").from("kkt_cookie_logs").where({"id_cookie": isiCookie});
        // console.log(`JUML Cookie ${recordsCookie.length}`);
        if(recordsCookie.length > 0) {
          let tempUserInfo = recordsCookie[0].res_meta;
          req.userInfo = {userinfo:tempUserInfo}
        }
        next();
    } else {
      console.log(`Check Auth, belum ada cookie!`);
      let result = new kktResp(203, "No authorized login");
      return res.status(203).send(result);
    }
  } catch (err) {
    console.log(err);
    const error = new HttpError("Authentication failed!", 403);
    return next(error);
  }
};
