class Snap_Resp {
    constructor(responseCode, responseMessage, referenceNo,partnerReferenceNo,accountToken,accessTokenInfo,linkId,nextAction,linkageToken,params, redirectUrl,userInfo) {
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
        this.referenceNo = referenceNo;
        this.partnerReferenceNo = partnerReferenceNo;
        this.accountToken = accountToken;
        this.accessTokenInfo = accessTokenInfo; //using 
        this.linkId = linkId;
        this.nextAction = nextAction;
        this.linkageToken = linkageToken;
        this.params = params;
        this.redirectUrl = redirectUrl;
        this.userInfo = userInfo;
      }

}

module.exports = Snap_Resp;