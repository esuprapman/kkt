



const randomByRange = async function between(min, max) {
    return Math.floor(
        Math.random() * (max - min + 1) + min
    )
}

const addMinutesByDate = async function addMinutes(date, minutes) {
    var result = new Date(date);
    console.log("TANGGAL ",date);
    result.setMinutes(result.getMinutes() + minutes);
    return result;
}
const addSecondsByDate = function addSeconds(date, seconds) {
    var result = new Date(date);
    result.setSeconds(result.getSeconds() + seconds);
    return result;
}

const addDaysByDate = function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
}

module.exports = {randomByRange, addSecondsByDate, addMinutesByDate, addDaysByDate};