const moment = require('moment-timezone');
const format2 = "YYMMDDHHmmss";
const pool = require("../connection/db");
const jwttools = require("../utils/encryptdecryptjwt");
const resCookieSign = require("../utils/cookieresponse");
const randomized = require("../utils/randomize");

const checkPassword = async (userinfo, password, resp)=> {
    // console.log("CHECK PASSWORD NIH ", JSON.stringify(userinfo));
    // console.log("PASSWORD ", password);
    if(userinfo.secret_user === password) {
        console.log("PASSWORD BENER ");
        // console.log(userinfo)
        const checkCookiesByUser = await pool.select("cl.*").from("kkt_cookie_logs as cl").where({"id_user_cookie": userinfo.id_user}).whereIn( "status_cookie",[1,3])
        console.log("###### PASSWORD CHECKING COOKIES ",checkCookiesByUser.length);
        if(checkCookiesByUser.length > 0) {
            console.log("UPDATE LOGGING ATTEMPT");
            let objectLog = checkCookiesByUser[0];
            let cookieResult = await resCookieSign.responseCookie200(resp,objectLog.id_cookie,3600, false,false); 
            const updateCookie = await pool("kkt_cookie_logs").update({status_cookie:1, loggin_attempt:objectLog.loggin_attempt+1},["id_cookie", "loggin_attempt"]).where({"id_cookie":objectLog.id_cookie});
            delete userinfo['secret_user'];
            delete userinfo['created_by'];
            delete userinfo['created_at'];
            delete userinfo['updated_at'];
            return {status:true, data:{msg:"login success!", attempt:updateCookie.loggin_attempt, data:userinfo}};
        } else {
            delete userinfo['secret_user'];delete userinfo['created_by'];
            delete userinfo['created_at'];delete userinfo['updated_at'];
            let idSession = await randomized.randomByRange(100000,999999);
            // let token = await jwttools.encryptdata(userinfo);
            let token = await jwttools.encryptdata(idSession);
            // console.log("###### baru : ",userinfo);
            const insertCookie = await pool("kkt_cookie_logs").insert({
                id_cookie: token,
                id_user_cookie: userinfo.id_user,
                created_by: userinfo.id_user,
                status_cookie: 1, //3 Means Error need relogin
                res_meta: userinfo,
                loggin_attempt: 1
            }, ["status_cookie"]);

            // console.log("Insert Cookie : ",userinfo);
            if(insertCookie.length > 0) {
                console.log("SIAPIN COOKIE");
                let cookieResult = await resCookieSign.responseCookie200(resp,token,3600, false,false); 
                return {status:true, data:{msg:"login success!", attempt:insertCookie.loggin_attempt, data:userinfo}};
            }
        }
    } else {
        //>>>>>>>>> Melihat Attempt yang dilakukan oleh Users
        const checkCookiesByUser = await pool.select("cl.*").from("kkt_cookie_logs as cl").where({"id_user_cookie": userinfo.id_user}).whereIn( "status_cookie",[1,3])
        if(checkCookiesByUser.length > 0) {
            console.log("UPDATE LOGGING ATTEMPT");
            let objectLog = checkCookiesByUser[0];
            console.log(JSON.stringify(objectLog));
            let cookieResult = await resCookieSign.responseCookie202(resp,objectLog.status_cookie,3600, false,false); 
            const updateCookie = await pool("kkt_cookie_logs").update({loggin_attempt:objectLog.loggin_attempt+1},["status_cookie", "loggin_attempt"]).where({"status_cookie":objectLog.status_cookie});
            // console.log(updateCookie);
            if(updateCookie.length > 0) {
                objectLog.loggin_attempt = updateCookie[0].loggin_attempt;
            }
            return {status:false, data:{msg:"Incorrect Password!", attempt:objectLog.loggin_attempt}};
        } else {
            delete userinfo['secret_user'];
            delete userinfo['created_by'];
            delete userinfo['created_at'];
            delete userinfo['updated_at'];
            let idSession = await randomized.randomByRange(100000,999999);
            // let token = await jwttools.encryptdata(userinfo);
            let token = await jwttools.encryptdata(idSession);
            const insertCookie = await pool("kkt_cookie_logs").insert({
                id_cookie: token,
                id_user_cookie: userinfo.id_user,
                created_by: userinfo.id_user,
                status_cookie: 3, //3 Means Error need relogin
                res_meta: userinfo,
                loggin_attempt: 1
            }, ["status_cookie"]);
            if(insertCookie.length > 0) {
                let cookieResult = await resCookieSign.responseCookie202(resp,token,3600, false,false); 
                return {status:false, data:{msg:"Incorrect Password!", attempt:1}};
            }
        }
    }

    
};

module.exports = {checkPassword};
