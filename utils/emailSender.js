const moment = require('moment-timezone');
const format2 = "YYMMDDHHmmss";
const pool = require("../connection/db");
const jwttools = require("../utils/encryptdecryptjwt");
const resCookieSign = require("../utils/cookieresponse");
const randomized = require("../utils/randomize");
const kktResp = require("../models/kkt_resp");
const nodemailer = require("nodemailer");
const checkSMTP = async (req, res, next) =>{
    let optionsArray=[];
    const checkSMTPParam = await pool.select("kmp.*").from("kkt_mst_params as kmp").where({"category_param": "KKTSMTP","type_param":0 });
    let status = checkSMTPParam.length > 0?200:203;
    if(status == 200) {
        // checkSMTPParam.map()
        await Promise.all(
            checkSMTPParam.map(async element2 =>{
                // arrayTmp.push(element2.id_menu);
                optionsArray[element2.name_param] = element2.value_param;
                // console.log(`${element2.name_param} = ${optionsArray[element2.name_param]}`);
            })
        )
        const transporter = nodemailer.createTransport({
            service: 'gmail',
            host: "smtp.gmail.com",
            port: 587,
            secure:false,
            auth: {
              user: optionsArray['USER'],
              pass: optionsArray['PASSWORD']
            }
          });
          const mailOptions = {
            from: optionsArray['USER'],
            to: 'muktiadhiryan@gmail.com',
            subject: 'Oncom TET',
            text: 'Email content'
          };
          transporter.sendMail(mailOptions, function(error, info){
              if (error) {
                  console.log(error);
                  return new kktResp(500, error);
              } else {
                  console.log('Email sent: ' + info.response);
                  return new kktResp(status, checkSMTPParam);
                  // do something useful
              }
          });
          return new kktResp(status, "Sending Message");
    } else {
        return new kktResp(status, checkSMTPParam);
    }   
};

const sendEmailFromKKT = async (req, res, next) =>{
    const {sendto,subject, message,htmltext} = req.body;
    let optionsArray=[];
    const checkSMTPParam = await pool.select("kmp.*").from("kkt_mst_params as kmp").where({"category_param": "KKTSMTP","type_param":0 });
    let status = checkSMTPParam.length > 0?200:203;
    if(status == 200) {
        // checkSMTPParam.map()
        await Promise.all(
            checkSMTPParam.map(async element2 =>{
                // arrayTmp.push(element2.id_menu);
                optionsArray[element2.name_param] = element2.value_param;
                // console.log(`${element2.name_param} = ${optionsArray[element2.name_param]}`);
            })
        )
        const transporter = nodemailer.createTransport({
            service: optionsArray['SERVICE'],
            host: optionsArray['HOST'],
            port: optionsArray['PORT'],
            secure:false,
            auth: {
              user: optionsArray['USER'],
              pass: optionsArray['PASSWORD']
            }
          });
          const mailOptions = {
            from: optionsArray['USER'],
            to: sendto,
            subject: subject,
            text: message,
            html:htmltext
          };
          transporter.sendMail(mailOptions, function(error, info){
              if (error) {
                  console.log(error);
                  return new kktResp(500, error);
              } else {
                  console.log('Email sent: ' + info.response);
                  return new kktResp(status, checkSMTPParam);
                  // do something useful
              }
          });
          return new kktResp(status, "Sending Message");
    } else {
        return new kktResp(status, checkSMTPParam);
    }   
};


module.exports = {checkSMTP, sendEmailFromKKT};