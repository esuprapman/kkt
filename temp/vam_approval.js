const express = require("express");
const router = express.Router();
const knex = require("../../connection/dbvamknex");
const pool = knex.conn();
const uuid = require("uuid");
const checkAuth = require("../../middleware/check-auth");
const vaUtil = require("../controller/vaNumber");
const companyUtil = require("../controller/company");
const kktAxios = require("../../connection/api/krakatoa");
const validateUtil = require("../controller/validateUtil");
router.use(checkAuth);

//get my task
router.post("/myTask", async (req, res, next) => {
  try {
    const token = req.headers.authorization;
    var userInfo = req.userData;
    const userBranch = userInfo.biokodecabang ? userInfo.biokodecabang : "0";
    var apps = userInfo.apps[0];
    const { tableName } = req.body;
    let realTable =
      tableName == "VA"
        ? "vam_van_master"
        : tableName == "Customer"
        ? "vam_vacustomer"
        : tableName == "COMPANY"
        ? "vam_company"
        : tableName == "PRODUCT"
        ? "vam_product"
        : tableName == "CHANNEL"
        ? "mst_channel"
        : tableName == "CONFIG"
        ? "mst_config"
        : tableName == "BRANCH"
        ? "mst_branch"
        : tableName == "FEE"
        ? "vam_fee_package"
        : tableName == "TEMPLATEFORM"
        ? "mst_template_form"
        : tableName == "NOTIF"
        ? "mst_template_notif"
        : tableName == "CORRECTION"
        ? "vam_vacorrection_header"
        : tableName == "AUTODEBET"
        ? "vam_auto_debet"
        : "vam_bulk";
    const approverData = {
      appId: apps.id,
      branchCode: userBranch,
      companyCode: userInfo?.idsubtenant > 0 ? userInfo.idsubtenant : null,
    };

    const data = await validateUtil.getApproval(realTable, approverData);
    return res.status(200).json({ status: 200, data: data });
  } catch (err) {
    console.log(err);
    return res.status(500).json({ status: 500, data: "Internal error" });
  }
});

module.exports = router;
