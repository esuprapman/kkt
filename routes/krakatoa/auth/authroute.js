const express = require("express"),
app = express();
const routerAuth = express.Router();
//######### lIST CONTROLLER ################################//
const namespace = '/adm/auth';
const authController = require("../../../controllers/krakatoa/auth/authcontroller");
// const checkAuth = require("../../middleware/check-auth");
// const checkAuthAttrb = require("../../middleware/check-auth-attribute");
//############### ROUTES FOR AUTHENTICATION VIA WEB / API ##############################
// routerAuth.post(`${namespace}/signvia`, authController.signVia);
routerAuth.post(`${namespace}/signvia`, async (req, res, next) => {
    const checkUsers = await authController.signVia(req, res, next);

    res.status(checkUsers.status).send(checkUsers);
});
routerAuth.post(`${namespace}/signviapp`, async (req, res, next) => {
    const checkUsers = await authController.signViaKeyApp(req, res, next);

    res.status(checkUsers.status).send(checkUsers);
});
routerAuth.post(`${namespace}/signout`, async (req, res, next) => {
    const checkUsers = await authController.signOut(req, res, next);
    if(checkUsers.status == 200) {
        res.clearCookie('x-cookie');
    }
    res.status(checkUsers.status).send(checkUsers);
});
routerAuth.get(`${namespace}/credaccess`, async (req, res, next) => {
    const checkUsers = await authController.checkCookiesPage(req, res, next);
    res.status(checkUsers.status).send(checkUsers);
});
module.exports = routerAuth;