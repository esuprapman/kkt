const express = require("express"),
app = express();
const routerModules = express.Router();

//######### lIST CONTROLLER ################################//
const namespace = '/modules';
const modulesController = require("../../../controllers/krakatoa/modules/modulescontroller");
const checkAuth = require("../../../middlewares/check-auth");
const checkCookie = require("../../../middlewares/check-cookies");
// const checkAuthAttrb = require("../../middleware/check-auth-attribute");
//############### ROUTES FOR AUTHENTICATION VIA WEB / API ##############################
routerModules.get(`${namespace}/retrivemodules`,checkCookie, async (req, res, next) => {
    const retriveModule = await modulesController.retriveModulesByCookie(req, res, next);
    res.status(retriveModule.status).send(retriveModule);
});
routerModules.get(`${namespace}/retrivemodulesNoChild`,checkCookie, async (req, res, next) => {
    const retriveModule = await modulesController.retriveModulesNoChild(req, res, next);
    res.status(retriveModule.status).send(retriveModule);
});
routerModules.post(`${namespace}/retrivemodulesAppId`,checkCookie, async (req, res, next) => {
    const retriveModule = await modulesController.retriveModuleByAppId(req, res, next);
    res.status(retriveModule.status).send(retriveModule);
});
routerModules.post(`${namespace}/editmodule`,checkCookie, async (req, res, next) => {
    const retriveApps = await modulesController.retriveModuleById(req, res, next);
    res.status(retriveApps.status).send(retriveApps);
});
routerModules.post(`${namespace}/addmodule`,checkCookie, async (req, res, next) => {
    const addModule = await modulesController.addModule(req, res, next);
    res.status(addModule.status).send(addModule);
});
routerModules.post(`${namespace}/delmodule`,checkCookie, async (req, res, next) => {
    const delModule = await modulesController.deleteModule(req, res, next);
    res.status(delModule.status).send(delModule);
});
routerModules.post(`${namespace}/updatemodule`,checkCookie, async (req, res, next) => {
    const updModule = await modulesController.updateModule(req, res, next);
    res.status(updModule.status).send(updModule);
});


//############################## EXTERNAL APLIKASI ###############
// routerModules.get(`${namespace}/attrb/:id`, checkAuth, async (req, res, next) => {
   
//     const retriveAttrb = await modulesController.retriveAttrByAppName(req, res, next);
//     console.log("Keluarannye ", retriveAttrb);
//     res.status(retriveAttrb.status).send(retriveAttrb);
// });

module.exports = routerModules;