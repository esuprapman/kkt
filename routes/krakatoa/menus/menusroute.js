const express = require("express"),
app = express();
const routermenus = express.Router();

//######### lIST CONTROLLER ################################//
const namespace = '/menus';
const menusController = require("../../../controllers/krakatoa/menus/menuscontrollers");
const checkAuth = require("../../../middlewares/check-auth");
const checkCookie = require("../../../middlewares/check-cookies");
// const checkAuthAttrb = require("../../middleware/check-auth-attribute");
//############### ROUTES FOR AUTHENTICATION VIA WEB / API ##############################
routermenus.get(`${namespace}/retrivemenus`,checkCookie, async (req, res, next) => {
    const retriveMenu = await menusController.retriveMenusByCookie(req, res, next);
    res.status(retriveMenu.status).send(retriveMenu);
});
routermenus.post(`${namespace}/retrivemenusbymoduleid`,checkCookie, async (req, res, next) => {
    const retriveMenu = await menusController.retriveMenuByModuleId(req, res, next);
    res.status(retriveMenu.status).send(retriveMenu);
});
routermenus.post(`${namespace}/editmenu`,checkCookie, async (req, res, next) => {
    const retriveApps = await menusController.retriveMenuById(req, res, next);
    res.status(retriveApps.status).send(retriveApps);
});
routermenus.post(`${namespace}/addmenu`,checkCookie, async (req, res, next) => {
    const addModule = await menusController.addMenu(req, res, next);
    res.status(addModule.status).send(addModule);
});
routermenus.post(`${namespace}/delmenu`,checkCookie, async (req, res, next) => {
    const delModule = await menusController.deleteMenu(req, res, next);
    res.status(delModule.status).send(delModule);
});
routermenus.post(`${namespace}/updatemenu`,checkCookie, async (req, res, next) => {
    const updModule = await menusController.updateMenu(req, res, next);
    res.status(updModule.status).send(updModule);
});
//############################## EXTERNAL APLIKASI ###############
// routermenus.get(`${namespace}/attrb/:id`, checkAuth, async (req, res, next) => {
   
//     const retriveAttrb = await menusController.retriveAttrByAppName(req, res, next);
//     console.log("Keluarannye ", retriveAttrb);
//     res.status(retriveAttrb.status).send(retriveAttrb);
// });

module.exports = routermenus;