const express = require("express"),
app = express();
const routergroups = express.Router();

//######### lIST CONTROLLER ################################//
const namespace = '/groups';
const groupsController = require("../../../controllers/krakatoa/groups/groupcontroller");
const checkAuth = require("../../../middlewares/check-auth");
const checkCookie = require("../../../middlewares/check-cookies");
// const checkAuthAttrb = require("../../middleware/check-auth-attribute");
//############### ROUTES FOR AUTHENTICATION VIA WEB / API ##############################
routergroups.get(`${namespace}/retrivegroups`,checkCookie, async (req, res, next) => {
    const retriveModule = await groupsController.retriveGroupsByCookie(req, res, next);
    res.status(retriveModule.status).send(retriveModule);
});
routergroups.get(`${namespace}/retrivegroups`,checkCookie, async (req, res, next) => {
    const retriveModule = await groupsController.retriveGroupsByCookie(req, res, next);
    res.status(retriveModule.status).send(retriveModule);
});
routergroups.post(`${namespace}/retrivegroupsmapid`,checkCookie, async (req, res, next) => {
    const retriveModule = await groupsController.retriveGroupByIdMap(req, res, next);
    res.status(retriveModule.status).send(retriveModule);
});
routergroups.post(`${namespace}/retrivegroupsuserid`,checkCookie, async (req, res, next) => {
    const retriveModule = await groupsController.retriveGroupByIdUser(req, res, next);
    res.status(retriveModule.status).send(retriveModule);
});
routergroups.post(`${namespace}/editgroup`,checkCookie, async (req, res, next) => {
    const retriveApps = await groupsController.retriveGroupById(req, res, next);
    res.status(retriveApps.status).send(retriveApps);
});
routergroups.post(`${namespace}/addgroup`,checkCookie, async (req, res, next) => {
    const addModule = await groupsController.addGroup(req, res, next);
    res.status(addModule.status).send(addModule);
});
routergroups.post(`${namespace}/addgroupmap`,checkCookie, async (req, res, next) => {
    const addModule = await groupsController.addGroupMap(req, res, next);
    res.status(addModule.status).send(addModule);
});
routergroups.post(`${namespace}/addgroupmenu`,checkCookie, async (req, res, next) => {
    const addModule = await groupsController.addGroupMenu(req, res, next);
    res.status(addModule.status).send(addModule);
});
routergroups.post(`${namespace}/addgroupusers`,checkCookie, async (req, res, next) => {
    const addModule = await groupsController.addGroupUser(req, res, next);
    res.status(addModule.status).send(addModule);
});
routergroups.post(`${namespace}/delgroup`,checkCookie, async (req, res, next) => {
    const delModule = await groupsController.deleteGroup(req, res, next);
    res.status(delModule.status).send(delModule);
});
routergroups.post(`${namespace}/delgroupmap`,checkCookie, async (req, res, next) => {
    const delModule = await groupsController.deleteGroupMap(req, res, next);
    res.status(delModule.status).send(delModule);
});
routergroups.post(`${namespace}/delgroupmenu`,checkCookie, async (req, res, next) => {
    const delModule = await groupsController.deleteGroupMenu(req, res, next);
    res.status(delModule.status).send(delModule);
});
routergroups.post(`${namespace}/delgroupuser`,checkCookie, async (req, res, next) => {
    const delModule = await groupsController.deleteGroupUser(req, res, next);
    res.status(delModule.status).send(delModule);
});


routergroups.post(`${namespace}/updategroup`,checkCookie, async (req, res, next) => {
    const updModule = await groupsController.updateGroup(req, res, next);
    res.status(updModule.status).send(updModule);
});


//############################## EXTERNAL APLIKASI ###############
// routergroups.get(`${namespace}/attrb/:id`, checkAuth, async (req, res, next) => {
   
//     const retriveAttrb = await groupsController.retriveAttrByAppName(req, res, next);
//     console.log("Keluarannye ", retriveAttrb);
//     res.status(retriveAttrb.status).send(retriveAttrb);
// });

module.exports = routergroups;