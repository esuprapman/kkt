const express = require("express"),
    app = express();
const routerCommon = express.Router();

//######### lIST CONTROLLER ################################//
const namespace = '/common';
const commonController = require("../../../controllers/krakatoa/commons/commonscontroller");
const checkAuth = require("../../../middlewares/check-auth");
const checkCookie = require("../../../middlewares/check-cookies");

const emailSender = require("../../../utils/emailSender");
// const checkAuthAttrb = require("../../middleware/check-auth-attribute");

//############### ROUTES FOR AUTHENTICATION VIA WEB / API ##############################
routerCommon.get(`${namespace}/retriveapps`, checkCookie, async (req, res, next) => {
    const retriveApps = await commonController.retriveAllApps(req, res, next);
    res.status(retriveApps.status).send(retriveApps);
});
routerCommon.get(`${namespace}/retriveroles`, checkCookie, async (req, res, next) => {
    const retriveApps = await commonController.retriveRoles(req, res, next);
    res.status(retriveApps.status).send(retriveApps);
});
routerCommon.post(`${namespace}/retrivebranchby`, checkCookie, async (req, res, next) => {
    const retriveApps = await commonController.retriveBranchByAppObject(req, res, next);
    res.status(retriveApps.status).send(retriveApps);
});
//############################## EXTERNAL APLIKASI ###############
// routerCommon.get(`${namespace}/attrb/:id`, checkAuth, async (req, res, next) => {
//     const retriveAttrb = await commonController.retriveAttrByAppName(req, res, next);
//     // console.log("Keluarannye ", retriveAttrb);
//     res.status(retriveAttrb.status).send(retriveAttrb);
// });
//############################# API COMMON ##############################################
routerCommon.get(`${namespace}/attrb/:id`, checkAuth, async (req, res, next) => {
    const retriveAttrb = await commonController.retriveAttrByKeyApp(req, res, next);
    // console.log("Keluarannye ", retriveAttrb);
    res.status(retriveAttrb.status).send(retriveAttrb);
});
routerCommon.post(`${namespace}/apicred`, checkAuth, async (req, res, next) => {
    const retriveAttrb = await commonController.retriveServiceByKeyApp(req, res, next);
    res.status(retriveAttrb.status).send(retriveAttrb);
});
routerCommon.get(`${namespace}/attrb`, checkAuth, async (req, res, next) => {
    const retriveAttrb = await commonController.retriveAttrNoApp(req, res, next);
    res.status(retriveAttrb.status).send(retriveAttrb);
});

routerCommon.post(`${namespace}/attrb`, checkAuth, async (req, res, next) => {
    const retriveAttrb = await commonController.retrivePageByKeyApp(req, res, next);
    res.status(retriveAttrb.status).send(retriveAttrb);
});





//############################# API LOGIN BY APP ##############################################

//############################# API COMMON ##############################################


//################################DIBUANG##########################

routerCommon.post(`${namespace}/postprofit`, async (req, res, next) => {
    const retriveAttrb = await commonController.postProfit(req, res, next);
    res.status(retriveAttrb.status).send(retriveAttrb);
});

routerCommon.get(`${namespace}/checkEmail`, async (req, res, next) => {
    const retriveAttrb = await emailSender.checkSMTP(req, res,next);
    res.status(retriveAttrb.status).send(retriveAttrb);
});

routerCommon.post(`${namespace}/sendEmailByAdmin`, async (req, res, next) => {
    const retriveAttrb = await emailSender.sendEmailFromKKT(req, res,next);
    res.status(retriveAttrb.status).send(retriveAttrb);
});

module.exports = routerCommon;