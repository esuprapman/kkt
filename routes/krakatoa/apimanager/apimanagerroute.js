const express = require("express"),
  app = express();
const routerapiMan = express.Router();

//######### lIST CONTROLLER ################################//
const namespace = "/apiman";
const apiManController = require("../../../controllers/krakatoa/apimanager/apimanagercontroller");
const checkAuth = require("../../../middlewares/check-auth");
const checkCookie = require("../../../middlewares/check-cookies");
// const checkAuthAttrb = require("../../middleware/check-auth-attribute");
//############### ROUTES FOR AUTHENTICATION VIA WEB / API ##############################
routerapiMan.get(
  `${namespace}/retriveapiman`,
  checkCookie,
  async (req, res, next) => {
    const retriveModule = await apiManController.retriveApiManByCookie(
      req,
      res,
      next
    );
    res.status(retriveModule.status).send(retriveModule);
  }
);
routerapiMan.get(
  `${namespace}/publiccommonapi`,
  checkCookie,
  async (req, res, next) => {
    const retriveModule = await apiManController.publicApiCommon(
      req,
      res,
      next
    );
    res.status(retriveModule.status).send(retriveModule);
  }
);
routerapiMan.get(
  `${namespace}/publicconsumeapi`,
  checkCookie,
  async (req, res, next) => {
    const retriveModule = await apiManController.publicApiConsume(
      req,
      res,
      next
    );
    res.status(retriveModule.status).send(retriveModule);
  }
);
routerapiMan.post(
  `${namespace}/retriveapimanbyid`,
  checkCookie,
  async (req, res, next) => {
    const retriveApis = await apiManController.retriveApiById(req, res, next);
    res.status(retriveApis.status).send(retriveApis);
  }
);

routerapiMan.post(
  `${namespace}/retriveapimanbyapiid`,
  checkCookie,
  async (req, res, next) => {
    const retriveApis = await apiManController.retriveApiByAPIId(
      req,
      res,
      next
    );
    res.status(retriveApis.status).send(retriveApis);
  }
);

routerapiMan.post(
  `${namespace}/publicexternalapi`,
  checkCookie,
  async (req, res, next) => {
    const retriveApis = await apiManController.publicApiExternal(
      req,
      res,
      next
    );
    res.status(retriveApis.status).send(retriveApis);
  }
);
routerapiMan.post(
  `${namespace}/addservice3api`,
  checkCookie,
  async (req, res, next) => {
    const addApis = await apiManController.addApi(req, res, next);
    res.status(addApis.status).send(addApis);
  }
);
routerapiMan.post(
  `${namespace}/delservice3api`,
  checkCookie,
  async (req, res, next) => {
    const delApis = await apiManController.deleteApi(req, res, next);
    res.status(delApis.status).send(delApis);
  }
);
routerapiMan.post(
  `${namespace}/updservice3api`,
  checkCookie,
  async (req, res, next) => {
    const updApis = await apiManController.updateApi(req, res, next);
    res.status(updApis.status).send(updApis);
  }
);
//############################## EXTERNAL APLIKASI ###############
// routerapiMan.get(`${namespace}/attrb/:id`, checkAuth, async (req, res, next) => {

//     const retriveAttrb = await apiManController.retriveAttrByAppName(req, res, next);
//     console.log("Keluarannye ", retriveAttrb);
//     res.status(retriveAttrb.status).send(retriveAttrb);
// });

module.exports = routerapiMan;
