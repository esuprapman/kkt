const express = require("express"),
app = express();
const routerusers = express.Router();

//######### lIST CONTROLLER ################################//
const namespace = '/users';
const usersController = require("../../../controllers/krakatoa/users/usercontroller");
const checkAuth = require("../../../middlewares/check-auth");
const checkCookie = require("../../../middlewares/check-cookies");
// const checkAuthAttrb = require("../../middleware/check-auth-attribute");
//############### ROUTES FOR AUTHENTICATION VIA WEB / API ##############################
routerusers.get(`${namespace}/retriveusers`,checkCookie, async (req, res, next) => {
    const retriveUsers = await usersController.retriveUsersByCookie(req, res, next);
    res.status(retriveUsers.status).send(retriveUsers);
});
routerusers.get(`${namespace}/retriveusersnogroup`,checkCookie, async (req, res, next) => {
    const retriveUsers = await usersController.retriveUserNotGrouped(req, res, next);
    res.status(retriveUsers.status).send(retriveUsers);
});
routerusers.post(`${namespace}/edituser`,checkCookie, async (req, res, next) => {
    const retriveUser = await usersController.retriveUserByUserId(req, res, next);
    res.status(retriveUser.status).send(retriveUser);
});
routerusers.post(`${namespace}/adduser`,checkCookie, async (req, res, next) => {
    const addUser = await usersController.addUser(req, res, next);
    res.status(addUser.status).send(addUser);
});
routerusers.post(`${namespace}/deluser`,checkCookie, async (req, res, next) => {
    const delUser = await usersController.deleteUser(req, res, next);
    res.status(delUser.status).send(delUser);
});
routerusers.post(`${namespace}/updateuser`,checkCookie, async (req, res, next) => {
    const updUser = await usersController.updateUser(req, res, next);
    res.status(updUser.status).send(updUser);
});


//############################## EXTERNAL APLIKASI ###############
// routerusers.get(`${namespace}/attrb/:id`, checkAuth, async (req, res, next) => {
   
//     const retriveAttrb = await usersController.retriveAttrByAppName(req, res, next);
//     console.log("Keluarannye ", retriveAttrb);
//     res.status(retriveAttrb.status).send(retriveAttrb);
// });

module.exports = routerusers;