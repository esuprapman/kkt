const express = require("express"),
app = express();
const routerCommon = express.Router();
const multer = require('multer');// Create multer object

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './public/assets/images/apps')
    },
    filename: function (req, file, cb) {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
        cb(null, uniqueSuffix + "-" + file.originalname);
    }
})
const imageUpload = multer({ storage: storage });
//######### lIST CONTROLLER ################################//
const namespace = '/application';
const applicationController = require("../../../controllers/krakatoa/applications/applicationcontroller");
const checkAuth = require("../../../middlewares/check-auth");
const checkCookie = require("../../../middlewares/check-cookies");
// const checkAuthAttrb = require("../../middleware/check-auth-attribute");
//############### ROUTES FOR AUTHENTICATION VIA WEB / API ##############################
routerCommon.get(`${namespace}/retriveapps`,checkCookie, async (req, res, next) => {
    const retriveApps = await applicationController.retriveAllApps(req, res, next);
    res.status(retriveApps.status).send(retriveApps);
});
routerCommon.post(`${namespace}/retriveappbykey`, async (req, res, next) => {
    const retriveApps = await applicationController.retriveAppsByKey(req, res, next);
    res.status(retriveApps.status).send(retriveApps);
});
routerCommon.post(`${namespace}/editapps`,checkCookie, async (req, res, next) => {
    const retriveApps = await applicationController.retriveAppsById(req, res, next);
    res.status(retriveApps.status).send(retriveApps);
});
routerCommon.post(`${namespace}/addapps`,checkCookie, async (req, res, next) => {
    const addApps = await applicationController.addApplication(req, res, next);
    res.status(addApps.status).send(addApps);
});
routerCommon.post(`${namespace}/delapps`,checkCookie, async (req, res, next) => {
    const delApps = await applicationController.deleteApplication(req, res, next);
    res.status(delApps.status).send(delApps);
});
routerCommon.post(`${namespace}/updateapps`,checkCookie, async (req, res, next) => {
    const updApps = await applicationController.updatApplication(req, res, next);
    res.status(updApps.status).send(updApps);
});



routerCommon.post(`${namespace}/uploadicon`,checkCookie,imageUpload.array("gambar",12), async (req, res) => {
    if (!req.files.empty) {
        res.status(200).json({status: 200,data: req.files,});
    } else {
        res.status(500).json({status: 500,data: "Error",});
    }
});

// {"id_app":"","name_app":"BULK-D","desc_app":"Aplikasi Dashbioard BI Fast","id_tenant_app":"","created_by":"","use_approve_app":{"code":0,"name":"False"},"link_app":"http://localhost:4201","path_icon":"http://localhost:3000/assets/images1680187802818-917771009-SeqDiagramLinkAja.png","key_app":"","status":{"code":1,"name":"True"}}

module.exports = routerCommon;