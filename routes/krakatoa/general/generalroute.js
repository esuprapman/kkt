const express = require("express"),
app = express();
const routerGeneral = express.Router();
const multer = require('multer');// Create multer object

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './public/assets/images')
    },
    filename: function (req, file, cb) {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
        cb(null, uniqueSuffix + "-" + file.originalname);
    }
})
const imageUpload = multer({ storage: storage });
//######### lIST CONTROLLER ################################//
const namespace = '/general';
const generalController = require("../../../controllers/krakatoa/general/generalcontroller");
const checkAuth = require("../../../middlewares/check-auth");
const checkCookie = require("../../../middlewares/check-cookies");
// const checkAuthAttrb = require("../../middleware/check-auth-attribute");
//############### ROUTES FOR AUTHENTICATION VIA WEB / API ##############################
routerGeneral.get(`${namespace}/retrivetenant`,checkCookie, async (req, res, next) => {
    const retriveTenant = await generalController.retriveTenantByCookie(req, res, next);
    res.status(retriveTenant.status).send(retriveTenant);
});

routerGeneral.get(`${namespace}/retriveNotenant`,checkCookie, async (req, res, next) => {
    const retriveTenant = await generalController.retriveTenantNoCookie(req, res, next);
    res.status(retriveTenant.status).send(retriveTenant);
});
routerGeneral.post(`${namespace}/uploadHead`,checkCookie,imageUpload.array("gambar",12), async (req, res) => {
    if (!req.files.empty) {
        res.status(200).json({status: 200,data: req.files,});
    } else {
        res.status(500).json({status: 500,data: "Error",});
    }
});
routerGeneral.post(`${namespace}/updategeneral`,checkCookie, async (req, res, next) => {
    const updataTenant = await generalController.updateTenant(req, res, next);
    res.status(updataTenant.status).send(updataTenant);
});

//############################## EXTERNAL APLIKASI ###############
// routerGeneral.get(`${namespace}/attrb/:id`, checkAuth, async (req, res, next) => {
//     const retriveAttrb = await generalController.retriveAttrByAppName(req, res, next);
//     console.log("Keluarannye ", retriveAttrb);
//     res.status(retriveAttrb.status).send(retriveAttrb);
// });

module.exports = routerGeneral;