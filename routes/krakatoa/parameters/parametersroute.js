const express = require("express"),
  app = express();
const routerparams = express.Router();

//######### lIST CONTROLLER ################################//
const namespace = "/params";
const paramsController = require("../../../controllers/krakatoa/parameters/parameterscontroller");
const checkCookie = require("../../../middlewares/check-cookies");
// const checkAuthAttrb = require("../../middleware/check-auth-attribute");
//############### ROUTES FOR AUTHENTICATION VIA WEB / API ##############################
routerparams.get(
  `${namespace}/retriveparams`,
  checkCookie,
  async (req, res, next) => {
    const retriveparams = await paramsController.retriveParametersByCookie(
      req,
      res,
      next
    );
    res.status(retriveparams.status).send(retriveparams);
  }
);

routerparams.post(
  `${namespace}/retrivebyappid`,
  checkCookie,
  async (req, res, next) => {
    const retriveApis = await paramsController.retriveParamByAppId(
      req,
      res,
      next
    );
    res.status(retriveApis.status).send(retriveApis);
  }
);

routerparams.post(
  `${namespace}/retrivebyparamid`,
  checkCookie,
  async (req, res, next) => {
    const retriveApis = await paramsController.retriveParamByParamId(
      req,
      res,
      next
    );
    res.status(retriveApis.status).send(retriveApis);
  }
);

routerparams.post(
  `${namespace}/addparam`,
  checkCookie,
  async (req, res, next) => {
    const addApis = await paramsController.addParameter(req, res, next);
    res.status(addApis.status).send(addApis);
  }
);
routerparams.post(
  `${namespace}/updateparam`,
  checkCookie,
  async (req, res, next) => {
    const delApis = await paramsController.updateParam(req, res, next);
    res.status(delApis.status).send(delApis);
  }
);
routerparams.post(
  `${namespace}/deleteparam`,
  checkCookie,
  async (req, res, next) => {
    const updApis = await paramsController.deleteParam(req, res, next);
    res.status(updApis.status).send(updApis);
  }
);

//############################## EXTERNAL APLIKASI ###############
// routerparams.get(`${namespace}/attrb/:id`, checkAuth, async (req, res, next) => {

//     const retriveAttrb = await paramsController.retriveAttrByAppName(req, res, next);
//     console.log("Keluarannye ", retriveAttrb);
//     res.status(retriveAttrb.status).send(retriveAttrb);
// });

module.exports = routerparams;
