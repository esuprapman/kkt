const https = require("https");
const fs = require("fs");
const morgan = require("morgan");
const express = require("express"),
  app = express(),
  port = process.env.port || 3000;
const cors = require("cors");
// origin: ['*','http://localhost:4200','http://localhost:4201'],
var corsOptions = {
  methods: ["GET", "POST", "DELETE", "UPDATE", "PUT", "PATCH"],
  origin: true, // sesuai sama
  credentials: true,
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
};
const cookieParser = require("cookie-parser");
// ################################ SET USING #####################################
app.use(morgan("dev"));
app.use(cors(corsOptions));
var path = require("path");
app.use(cookieParser("AjinomotoCapMangkokMerah"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
//-==================== PATH SETTING =================
app.use(express.static(path.join(__dirname, "public")));
//======================================================//

//-==================== Set Routing =================
const routesAuthApi = require("./routes/krakatoa/auth/authroute");
const routesCommonApi = require("./routes/krakatoa/commons/commonroute");
const routesGenerralApi = require("./routes/krakatoa/general/generalroute");
const routesAplicationManagerApi = require("./routes/krakatoa/applications/applicationroute");
const routesModulesManagerApi = require("./routes/krakatoa/modules/modulesroute");
const routesMenusManagerApi = require("./routes/krakatoa/menus/menusroute");
const routesGroupManagerApi = require("./routes/krakatoa/groups/groupsroute");
const routesUsersManagerApi = require("./routes/krakatoa/users/userroutes");
const routesApiManagerApi = require("./routes/krakatoa/apimanager/apimanagerroute");
const routesParameterApi = require("./routes/krakatoa/parameters/parametersroute");
//==========================================================

//===================== Set Usage API ==================
app.use(routesAuthApi);
app.use(routesCommonApi);
app.use(routesGenerralApi);
app.use(routesAplicationManagerApi);
app.use(routesModulesManagerApi);
app.use(routesMenusManagerApi);
app.use(routesGroupManagerApi);
app.use(routesUsersManagerApi);
app.use(routesApiManagerApi);
app.use(routesParameterApi);
//^^^^^^^^^^^^^^^^^^^^ SUPAYA BISA PATH LOCATION STRATEGU
app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname, "/public/index.html"));
});
var server = app.listen(port, function () {
  let host = server.address().address;
  let portname = server.address().port;
  console.log("KKT Server is running on http://%s:%s", host, portname);
});
